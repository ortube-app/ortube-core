package com.ortube.mail.service.impl

import com.ortube.mail.data.MailSource
import com.ortube.mail.service.CssInliner
import org.fit.cssbox.css.CSSNorm
import org.fit.cssbox.css.DOMAnalyzer
import org.fit.cssbox.css.NormalOutput
import org.fit.cssbox.io.DefaultDOMSource
import org.springframework.stereotype.Service
import java.io.ByteArrayOutputStream

@Service
class CssInlinerImpl : CssInliner {

    override fun inlineCss(html: String): String {
        val document = DefaultDOMSource(MailSource(html)).parse().also {
            val domAnalyzer = DOMAnalyzer(it)
            domAnalyzer.attributesToStyles() // convert the HTML presentation attributes to inline styles
            domAnalyzer.addStyleSheet(null, CSSNorm.stdStyleSheet(), DOMAnalyzer.Origin.AGENT) // use the standard style sheet
            domAnalyzer.addStyleSheet(null, CSSNorm.userStyleSheet(), DOMAnalyzer.Origin.AGENT) // use the additional style sheet
            domAnalyzer.getStyleSheets() // load the author style sheets

            domAnalyzer.stylesToDomInherited()
        }

        val os = ByteArrayOutputStream().apply {
            NormalOutput(document).dumpTo(this)
            close()
        }

        return String(os.toByteArray())
    }

}