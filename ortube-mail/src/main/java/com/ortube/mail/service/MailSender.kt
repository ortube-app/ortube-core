package com.ortube.mail.service

import com.ortube.mail.data.MailDto

interface MailSender {
    fun sendEmail(mailDto: MailDto)
}