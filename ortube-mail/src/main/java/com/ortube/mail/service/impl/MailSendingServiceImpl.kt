package com.ortube.mail.service.impl

import com.ortube.mail.constants.REGISTRATION_SUBJECT
import com.ortube.mail.data.MailDto
import com.ortube.mail.service.MailMessageProvider
import com.ortube.mail.service.MailSender
import com.ortube.mail.service.MailSendingService
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service

@Service
class MailSendingServiceImpl(
    private val mailMessageProvider: MailMessageProvider,
    private val mailSender: MailSender,

    @Value("\${app.activationLink}")
    private val activationLink: String
) : MailSendingService {

    @Async
    override fun sendActivationEmail(email: String, nickname: String, activationCode: String) {
        val emailBody = mailMessageProvider.provideMessageBody("registration.ftl", mapOf(
                "nickname" to nickname,
                "activationLink" to "$activationLink$activationCode"
        ))
        val mail = MailDto(email, REGISTRATION_SUBJECT, emailBody)

        mailSender.sendEmail(mail)
    }

}