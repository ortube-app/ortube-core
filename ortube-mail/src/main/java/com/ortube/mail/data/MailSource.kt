package com.ortube.mail.data

import org.fit.cssbox.io.DocumentSource

class MailSource(private val body: String) : DocumentSource(null) {

    override fun getURL() = throw NotImplementedError()

    override fun close() = throw NotImplementedError()

    override fun getContentType() = "text/html; charset=utf-8"

    override fun getInputStream() = body.byteInputStream()

}