package com.ortube.mail.data

data class MailDto(
    val sendTo: String,
    val subject: String,
    val body: String
)