FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /root/app
COPY ortube-web/target/'ortube-web-0.0.1-SNAPSHOT-exec.jar' /root/app/ortube.jar

EXPOSE 8080

CMD java $JAVA_OPTS -jar /root/app/ortube.jar

