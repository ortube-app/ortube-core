<?xml version="1.0"?>
<!DOCTYPE module PUBLIC
        "-//Puppy Crawl//DTD Check Configuration 1.3//EN"
        "http://www.puppycrawl.com/dtds/configuration_1_3.dtd">

<module name="Checker">

    <property name="fileExtensions" value="java, properties, xml, json, yml"/>
    <property name="charset" value="UTF-8"/>

    <module name="SuppressWarningsFilter"/>

    <!-- Checks for Size Violations                     -->
    <!-- See http://checkstyle.sf.net/config_sizes.html -->
    <module name="FileLength"/>
    <module name="LineLength">
        <property name="max" value="130"/>
    </module>

    <!-- Checks for whitespace                               -->
    <!-- See http://checkstyle.sf.net/config_whitespace.html -->
    <module name="FileTabCharacter"/>

    <!-- Miscellaneous other checks.                   -->
    <!-- See http://checkstyle.sf.net/config_misc.html -->
    <module name="RegexpSingleline">
        <property name="format" value="\s+$"/>
        <property name="message" value="Line has trailing spaces."/>
    </module>
    <module name="RegexpMultiline">
        <property name="format" value="System\.(out)|(err)\.print(ln)?\("/>
        <property name="message" value="Avoid using default console output."/>
    </module>

    <module name="TreeWalker">

        <!-- Check location of annotation on language elements   -->
        <!-- See http://checkstyle.sf.net/config_annotation.html -->
        <module name="SuppressWarningsHolder"/>
        <module name="MissingOverride"/>

        <!-- Checks for Naming Conventions                   -->
        <!-- See http://checkstyle.sf.net/config_naming.html -->
        <module name="AbbreviationAsWordInName">
            <property name="allowedAbbreviationLength" value="5"/>
        </module>
        <module name="AbstractClassName"/>
        <module name="ConstantName"/>
        <module name="ClassTypeParameterName"/>
        <module name="InterfaceTypeParameterName"/>
        <module name="LambdaParameterName"/>
        <module name="LocalVariableName"/>
        <module name="MemberName"/>
        <module name="MethodName"/>
        <module name="MethodTypeParameterName"/>
        <module name="PackageName">
            <property name="format" value="^[a-z]+(\.[a-z0-9]*)*$"/>
        </module>
        <module name="ParameterName"/>
        <module name="StaticVariableName"/>
        <module name="TypeName"/>

        <!-- Checks for imports                              -->
        <!-- See http://checkstyle.sf.net/config_import.html -->
        <module name="AvoidStarImport"/>
        <module name="IllegalImport"/> <!-- defaults to sun.* packages -->
        <module name="RedundantImport"/>
        <module name="UnusedImports"/>

        <!-- Checks for Size Violations.                    -->
        <!-- See http://checkstyle.sf.net/config_sizes.html -->
        <module name="MethodCount"/>
        <module name="MethodLength"/>
        <module name="ParameterNumber"/>

        <!-- Checks for whitespace                               -->
        <!-- See http://checkstyle.sf.net/config_whitespace.html -->
        <module name="EmptyForInitializerPad"/>
        <module name="EmptyForIteratorPad"/>
        <module name="EmptyLineSeparator">
            <property name="tokens"
                      value="STATIC_IMPORT, CLASS_DEF, INTERFACE_DEF, ENUM_DEF, STATIC_INIT, INSTANCE_INIT, METHOD_DEF, CTOR_DEF, VARIABLE_DEF"/>
            <property name="allowNoEmptyLineBetweenFields" value="true"/>
            <property name="allowMultipleEmptyLines" value="false"/>
        </module>
        <module name="GenericWhitespace"/>
        <module name="MethodParamPad"/>
        <module name="NoLineWrap"/>
        <module name="NoWhitespaceAfter"/>
        <module name="NoWhitespaceBefore"/>
        <module name="OperatorWrap"/>
        <module name="ParenPad"/>
        <module name="SeparatorWrap">
            <property name="tokens" value="DOT"/>
            <property name="option" value="nl"/>
        </module>
        <module name="SeparatorWrap">
            <property name="tokens" value="COMMA, SEMI, ELLIPSIS, LPAREN, ARRAY_DECLARATOR, RBRACK, METHOD_REF"/>
            <property name="option" value="eol"/>
        </module>
        <module name="SingleSpaceSeparator"/>
        <module name="TypecastParenPad"/>
        <module name="WhitespaceAfter"/>
        <module name="WhitespaceAround"/>

        <!-- Modifier Checks                                   -->
        <!-- See http://checkstyle.sf.net/config_modifier.html -->
        <module name="ModifierOrder"/>
        <module name="RedundantModifier"/>

        <!-- Checks for blocks. You know, those {}'s         -->
        <!-- See http://checkstyle.sf.net/config_blocks.html -->
        <module name="AvoidNestedBlocks"/>
        <module name="EmptyBlock"/>
        <module name="EmptyCatchBlock"/>
        <module name="LeftCurly"/>
        <module name="NeedBraces"/>
        <module name="RightCurly"/>

        <!-- Checks for common coding problems               -->
        <!-- See http://checkstyle.sf.net/config_coding.html -->
        <module name="DeclarationOrder"/>
        <module name="DefaultComesLast"/>
        <module name="EmptyStatement"/>
        <module name="EqualsAvoidNull"/> <!-- probably unnecessary -->
        <module name="EqualsHashCode"/>
        <module name="FallThrough"/>
        <module name="IllegalCatch"/>
        <module name="IllegalInstantiation"/>
        <module name="IllegalThrows"/>
        <module name="IllegalToken"/>
        <module name="InnerAssignment"/>
        <module name="MissingSwitchDefault"/>
        <module name="MultipleStringLiterals">
            <property name="allowedDuplicates" value="4"/>
            <property name="ignoreStringsRegexp" value='\$\.'/>
        </module>
        <module name="MultipleVariableDeclarations"/>
        <module name="NoFinalizer"/>
        <module name="OneStatementPerLine"/>
        <module name="OverloadMethodsDeclarationOrder"/>
        <module name="SimplifyBooleanExpression"/>
        <module name="SimplifyBooleanReturn"/>
        <module name="UnnecessaryParentheses"/>

        <!-- Checks for class design                         -->
        <!-- See http://checkstyle.sf.net/config_design.html -->
        <module name="FinalClass"/>
        <module name="InterfaceIsType"/>
        <module name="VisibilityModifier">
            <property name="protectedAllowed" value="true"/>
            <property name="allowPublicFinalFields" value="true"/>
            <property name="allowPublicImmutableFields" value="true"/>
            <property name="ignoreAnnotationCanonicalNames"
                      value="com.google.common.annotations.VisibleForTesting, org.junit.jupiter.api.Test"/>
        </module>
        <module name="OneTopLevelClass"/>

        <!-- Miscellaneous other checks                    -->
        <!-- See http://checkstyle.sf.net/config_misc.html -->
        <module name="ArrayTypeStyle"/>
        <module name="UpperEll"/>
        <module name="Indentation"/>
        <module name="AvoidEscapedUnicodeCharacters"/>

    </module>

</module>
