package com.ortube.config

import com.ortube.datagenerators.PlaylistDataGenerator
import com.ortube.datagenerators.UserDataGenerator
import com.ortube.datagenerators.UserPlaylistDataGenerator
import com.ortube.datagenerators.WordDataGenerator
import com.ortube.persistence.domain.Playlist
import com.ortube.persistence.domain.User
import com.ortube.persistence.domain.UserPlaylist
import com.ortube.persistence.repository.PlaylistRepository
import com.ortube.persistence.repository.UserPlaylistRepository
import com.ortube.persistence.repository.UserRepository
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = [DomainTestConfig::class])
@ActiveProfiles("integration-test")
abstract class AbstractDomainIntegrationTest {

    @Autowired
    lateinit var mongoTemplate: MongoTemplate

    @Autowired
    lateinit var playlistRepository: PlaylistRepository

    @Autowired
    lateinit var userPlaylistRepository: UserPlaylistRepository

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var playlistDataGenerator: PlaylistDataGenerator

    @Autowired
    lateinit var userPlaylistDataGenerator: UserPlaylistDataGenerator

    @Autowired
    lateinit var userDataGenerator: UserDataGenerator

    @Autowired
    lateinit var wordDataGenerator: WordDataGenerator

    @BeforeEach
    fun setUp() {
        mongoTemplate.dropCollection(Playlist::class.java)
        mongoTemplate.dropCollection(UserPlaylist::class.java)
        mongoTemplate.dropCollection(User::class.java)
    }
}