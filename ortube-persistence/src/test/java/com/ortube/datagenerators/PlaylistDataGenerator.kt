package com.ortube.datagenerators

import com.ortube.persistence.domain.Playlist
import com.ortube.persistence.domain.Word
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.bson.types.ObjectId
import org.springframework.boot.test.context.TestComponent
import org.springframework.context.annotation.Profile
import org.springframework.data.mongodb.core.MongoTemplate

@TestComponent
@Profile("integration-test")
class PlaylistDataGenerator(
    private val wordDataGenerator: WordDataGenerator,
    private val mongoTemplate: MongoTemplate
) {
    fun playlist(
        words: List<Word> = wordDataGenerator.words().onEach { it.id = ObjectId.get().toHexString() }
    ): Playlist {
        val playlist = Playlist(
                title = randomAlphabetic(30),
                topic = randomAlphabetic(30),
                words = words
        )
        return mongoTemplate.save(playlist)
    }
}