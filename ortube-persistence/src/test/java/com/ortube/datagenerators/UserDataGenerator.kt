package com.ortube.datagenerators

import com.ortube.persistence.domain.User
import com.ortube.persistence.domain.enums.UserRole
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.springframework.boot.test.context.TestComponent
import org.springframework.context.annotation.Profile
import org.springframework.data.mongodb.core.MongoTemplate

@TestComponent
@Profile("integration-test")
class UserDataGenerator(
    private val mongoTemplate: MongoTemplate
) {

    fun user(): User {
        val user = User(
                email = randomAlphabetic(20) + "@gmail.com",
                password = randomAlphabetic(20),
                nickname = randomAlphabetic(20),
                username = randomAlphabetic(20),
                userRole = UserRole.USER,
                image = randomAlphabetic(20)
        )
        return mongoTemplate.save(user)
    }

}