package com.ortube.datagenerators

import com.ortube.persistence.domain.UserPlaylist
import com.ortube.persistence.domain.Word
import com.ortube.persistence.domain.enums.PlaylistPrivacy
import org.apache.commons.lang3.RandomStringUtils
import org.bson.types.ObjectId
import org.springframework.boot.test.context.TestComponent
import org.springframework.context.annotation.Profile
import org.springframework.data.mongodb.core.MongoTemplate

@TestComponent
@Profile("integration-test")
class UserPlaylistDataGenerator(
    private val mongoTemplate: MongoTemplate,
    private val wordDataGenerator: WordDataGenerator
) {

    fun userPlaylist(
        userId: String = ObjectId.get().toHexString(),
        privacy: PlaylistPrivacy = PlaylistPrivacy.PUBLIC,
        words: List<Word> = wordDataGenerator.words(5).apply { forEach { it.id = ObjectId.get().toHexString() } }
    ): UserPlaylist {

        val userPlaylist = UserPlaylist(
                title = RandomStringUtils.randomAlphabetic(30),
                topic = RandomStringUtils.randomAlphabetic(30),
                privacy = privacy,
                userId = userId,
                words = words
        )
        return mongoTemplate.save(userPlaylist)
    }

    fun publicPlaylistForUser(userId: String) = userPlaylist(userId, PlaylistPrivacy.PUBLIC)

    fun privatePlaylistForUser(userId: String) = userPlaylist(userId, PlaylistPrivacy.PRIVATE)

}