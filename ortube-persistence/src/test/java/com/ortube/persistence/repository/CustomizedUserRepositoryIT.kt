package com.ortube.persistence.repository

import com.ortube.config.AbstractDomainIntegrationTest
import com.ortube.divRoundUp
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CustomizedUserRepositoryIT : AbstractDomainIntegrationTest() {

    @Test
    fun `Should find user projection by id`() {
        // GIVEN
        val user = userDataGenerator.user()

        // WHEN
        val userOptional = userRepository.findProjectionById(user.id!!)

        // THEN
        assertThat(userOptional).isNotEmpty
        val userProjection = userOptional.get()
        assertThat(userProjection.username).isEqualTo(user.username)
        assertThat(userProjection.nickname).isEqualTo(user.nickname)
        assertThat(userProjection.image).isEqualTo(user.image)
        assertThat(userProjection.id).isEqualTo(user.id)
        assertThat(userProjection.playlistCount).isZero()
    }

    @Test
    fun `Should count all user's public playlists`() {
        // GIVEN
        val (userId) = userDataGenerator.user()

        val publicPlaylists = listOf(
                userPlaylistDataGenerator.publicPlaylistForUser(userId!!),
                userPlaylistDataGenerator.publicPlaylistForUser(userId)
        )

        userPlaylistDataGenerator.privatePlaylistForUser(userId)

        // WHEN
        val userProjection = userRepository.findProjectionById(userId).get()

        // THEN
        assertThat(userProjection.playlistCount).isEqualTo(publicPlaylists.size)
    }

    @Test
    fun `Should return empty optional if id is invalid`() {
        // GIVEN
        val id = "123"

        // WHEN
        val userProjection = userRepository.findProjectionById(id)

        // THEN
        assertThat(userProjection).isEmpty
    }

    @Test
    fun `Should return empty optional if user by id not found`() {
        // GIVEN
        val id = "45cbc4a0e4123f6920000002"

        // WHEN
        val userProjection = userRepository.findProjectionById(id)

        // THEN
        assertThat(userProjection).isEmpty
    }

    @Test
    fun `Should find all user's projections`() {
        // GIVEN
        val users = generateSequence { userDataGenerator.user() }.take(23).toList()

        // WHEN
        val projections = userRepository.findAllCommonProjections(0, 100)

        // THEN
        assertThat(projections.content).hasSameSizeAs(users)
    }

    @Test
    fun `Should return first 5 records when page is 0 and size is 5`() {
        // GIVEN
        val users = generateSequence { userDataGenerator.user() }.take(23).toList()
        val size = 5

        // WHEN
        val projections = userRepository.findAllCommonProjections(0, size)

        // THEN
        assertThat(projections.content).hasSize(size)
        assertThat(projections.isLast).isFalse()
        assertThat(projections.isFirst).isTrue()
        assertThat(projections.totalElements.toInt()).isEqualTo(users.size)
        assertThat(projections.totalPages).isEqualTo(users.size.divRoundUp(size))
        assertThat(projections.content.map { it.id }).isEqualTo(users.take(size).map { it.id })
    }

    @Test
    fun `Should return second 4 records when page is 1 and size is 4`() {
        // GIVEN
        val users = generateSequence { userDataGenerator.user() }.take(23).toList()
        val size = 4

        // WHEN
        val projections = userRepository.findAllCommonProjections(1, size)

        // THEN
        assertThat(projections.content).hasSize(size)
        assertThat(projections.isLast).isFalse()
        assertThat(projections.isFirst).isFalse()
        assertThat(projections.totalElements.toInt()).isEqualTo(users.size)
        assertThat(projections.totalPages).isEqualTo(users.size.divRoundUp(size))
        assertThat(projections.content.map { it.id }).isEqualTo(users.drop(size).take(size).map { it.id })
    }

}