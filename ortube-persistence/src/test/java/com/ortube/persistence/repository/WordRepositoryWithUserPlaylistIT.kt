package com.ortube.persistence.repository

import com.ortube.config.AbstractDomainIntegrationTest
import com.ortube.deepClone
import com.ortube.persistence.domain.UserPlaylist
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

class WordRepositoryWithUserPlaylistIT : AbstractDomainIntegrationTest() {

    @Autowired
    lateinit var wordRepository: WordRepository<UserPlaylist>

    @Test
    fun `Should add word to playlist`() {
        // GIVEN
        val originalPlaylist = userPlaylistDataGenerator.userPlaylist(words = emptyList())
        val newWord = wordDataGenerator.word()

        // WHEN
        wordRepository.addWord(originalPlaylist.id!!, newWord.deepClone(), UserPlaylist::class.java)
        val actualWord = mongoTemplate.findById(originalPlaylist.id!!, UserPlaylist::class.java)!!.words[0]

        // THEN
        assertNotNull(actualWord.id)
        assertEquals(newWord.lang1, actualWord.lang1)
        assertEquals(newWord.lang2, actualWord.lang2)
    }

    @Test
    fun `Should update playlist last modified date after adding word`() {
        // GIVEN
        val originalPlaylist = userPlaylistDataGenerator.userPlaylist()
        val newWord = wordDataGenerator.word()

        // WHEN
        wordRepository.addWord(originalPlaylist.id!!, newWord, UserPlaylist::class.java)
        val updatedPlaylist = mongoTemplate.findById(originalPlaylist.id!!, UserPlaylist::class.java)

        // THEN
        assertNotEquals(originalPlaylist.lastModifiedDate, updatedPlaylist!!.lastModifiedDate)
        assertThat(updatedPlaylist.lastModifiedDate).isAfter(originalPlaylist.lastModifiedDate)
    }

    @Test
    fun `Should update word`() {
        // GIVEN
        val playlist = userPlaylistDataGenerator.userPlaylist()
        val originalWord = playlist.words[0]
        val newWord = wordDataGenerator.word()

        // WHEN
        wordRepository.update(playlist.id!!, originalWord.id!!, newWord, UserPlaylist::class.java)

        val actualWord = mongoTemplate
                .findById(playlist.id!!, UserPlaylist::class.java)!!
                .words.first { it.id == originalWord.id }

        // THEN
        assertEquals(newWord.lang1, actualWord.lang1)
        assertEquals(newWord.lang2, actualWord.lang2)
        assertNotEquals(originalWord.lang1, actualWord.lang1)
        assertNotEquals(originalWord.lang2, actualWord.lang2)
        assertEquals(originalWord.id, actualWord.id)
    }

    @Test
    fun `Should update playlist last modified date after updating word`() {
        // GIVEN
        val originalPlaylist = userPlaylistDataGenerator.userPlaylist()
        val newWord = wordDataGenerator.word()

        // WHEN
        wordRepository.update(originalPlaylist.id!!, originalPlaylist.words[0].id!!, newWord, UserPlaylist::class.java)
        val updatedPlaylist = mongoTemplate.findById(originalPlaylist.id!!, UserPlaylist::class.java)

        // THEN
        assertNotEquals(originalPlaylist.lastModifiedDate, updatedPlaylist!!.lastModifiedDate)
        assertThat(updatedPlaylist.lastModifiedDate).isAfter(originalPlaylist.lastModifiedDate)
    }

    @Test
    fun `Should delete word by id and playlist id`() {
        // GIVEN
        val originalPlaylist = userPlaylistDataGenerator.userPlaylist()
        val originalWord = originalPlaylist.words[0]

        // WHEN
        wordRepository.deleteByIdAndPlaylistId(originalWord.id!!, originalPlaylist.id!!, UserPlaylist::class.java)
        val actualPlaylist = mongoTemplate.findById(originalPlaylist.id!!, UserPlaylist::class.java)

        // THEN
        assertThat(actualPlaylist!!.words).hasSize(originalPlaylist.words.size - 1)
        assertThat(actualPlaylist.words).doesNotContain(originalWord)
    }

    @Test
    fun `Should delete all words by ids and playlist id`() {
        // GIVEN
        val originalPlaylist = userPlaylistDataGenerator.userPlaylist()

        val wordsToDelete = listOf(originalPlaylist.words[0], originalPlaylist.words[1])

        // WHEN
        wordRepository.deleteAllByIdAndPlaylistId(
                wordsToDelete.map { it.id!! },
                originalPlaylist.id!!, UserPlaylist::class.java
        )
        val actualPlaylist = mongoTemplate.findById(originalPlaylist.id!!, UserPlaylist::class.java)

        // THEN
        assertThat(actualPlaylist!!.words).hasSize(originalPlaylist.words.size - wordsToDelete.size)
        assertThat(actualPlaylist.words).doesNotContainAnyElementsOf(wordsToDelete)
    }
}