package com.ortube.persistence.repository

import com.ortube.config.AbstractDomainIntegrationTest
import com.ortube.persistence.domain.UserPlaylist
import com.ortube.persistence.domain.enums.PlaylistPrivacy
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import java.util.function.Consumer

class CustomizedUserPlaylistRepositoryIT : AbstractDomainIntegrationTest() {

    @Test
    fun `Should generate ids for words in playlist while saving`() {
        // GIVEN
        val userPlaylist = userPlaylistDataGenerator.userPlaylist()

        // WHEN
        val savedPlaylist = userPlaylistRepository.save(userPlaylist)

        // THEN
        savedPlaylist.words.forEach(Consumer { (id) -> assertNotNull(id) })
    }

    @Test
    fun `Should update all new fields`() {

        // GIVEN
        val userPlaylist = userPlaylistDataGenerator.userPlaylist()
        val newTitle = randomAlphabetic(30)
        val newTopic = randomAlphabetic(30)
        val newPrivacy = PlaylistPrivacy.PRIVATE

        val updates = UserPlaylist(
                privacy = newPrivacy,
                title = newTitle,
                topic = newTopic,
                userId = ""
        )

        // WHEN
        val updatedPlaylist = userPlaylistRepository.update(
                userPlaylist.id!!,
                updates,
                userPlaylist.userId
        )

        // THEN
        assertEquals(newTitle, updatedPlaylist.title)
        assertEquals(newTopic, updatedPlaylist.topic)
        assertEquals(newPrivacy.name, updatedPlaylist.privacy.name)
        assertEquals(userPlaylist.id, updatedPlaylist.id)
        assertEquals(userPlaylist.userId, updatedPlaylist.userId)
        assertEquals(userPlaylist.words, updatedPlaylist.words)
        assertThat(updatedPlaylist.createdDate).isEqualToIgnoringNanos(userPlaylist.createdDate)
        assertThat(updatedPlaylist.lastModifiedDate).isAfter(userPlaylist.lastModifiedDate)
    }
}