package com.ortube.persistence.repository

import com.ortube.config.AbstractDomainIntegrationTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test

class CustomizedPlaylistRepositoryIT : AbstractDomainIntegrationTest() {

    @Test
    fun `Should generate ids for words in playlist while saving`() {
        // GIVEN
        val playlist = playlistDataGenerator.playlist()

        // WHEN
        val savedPlaylist = playlistRepository.save(playlist)

        // THEN
        savedPlaylist.words.forEach { assertNotNull(it.id) }
    }

    @Test
    fun `Should update playlist`() {
        // GIVEN
        val playlist = playlistDataGenerator.playlist()
        val playlistUpdates = playlistDataGenerator.playlist()

        // WHEN
        val updatedPlaylist = playlistRepository.update(playlist.id!!, playlistUpdates)

        // THEN
        assertEquals(playlist.id, updatedPlaylist.id)
        assertEquals(playlistUpdates.title, updatedPlaylist.title)
        assertEquals(playlistUpdates.topic, updatedPlaylist.topic)
        assertEquals(playlist.words, updatedPlaylist.words)
        assertThat(updatedPlaylist.lastModifiedDate).isAfter(playlist.lastModifiedDate)
    }

}