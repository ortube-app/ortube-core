package com.ortube

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import java.util.Optional

private val mapper = jacksonObjectMapper().registerModule(JavaTimeModule())!!

fun <T : Any> T.deepClone() = mapper.readValue(mapper.writeValueAsString(this), this.javaClass)!!

fun <T> T.toOptional() = Optional.of(this)

fun Int.divRoundUp(other: Int) = (this + other - 1) / other
