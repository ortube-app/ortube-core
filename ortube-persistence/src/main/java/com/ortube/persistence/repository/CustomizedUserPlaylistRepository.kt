package com.ortube.persistence.repository

import com.ortube.persistence.domain.UserPlaylist

interface CustomizedUserPlaylistRepository {

    fun update(playlistId: String, userPlaylist: UserPlaylist, userId: String): UserPlaylist

}