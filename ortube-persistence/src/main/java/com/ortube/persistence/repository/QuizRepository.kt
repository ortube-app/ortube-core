package com.ortube.persistence.repository

import com.ortube.persistence.domain.Quiz
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.Optional

interface QuizRepository : MongoRepository<Quiz, String> {

    fun findByUserIdAndIdAndFinishedIsFalse(userId: String, quizId: String): Optional<Quiz>

}