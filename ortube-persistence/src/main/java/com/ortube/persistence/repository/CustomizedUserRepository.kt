package com.ortube.persistence.repository

import com.ortube.persistence.domain.projection.CommonUserProjection
import org.springframework.data.domain.Page
import java.util.Optional

interface CustomizedUserRepository {

    fun findProjectionById(id: String): Optional<CommonUserProjection>

    fun findAllCommonProjections(page: Int, size: Int): Page<CommonUserProjection>

}