package com.ortube.persistence.repository.impl

import com.ortube.persistence.domain.UserPlaylist
import com.ortube.persistence.repository.CustomizedUserPlaylistRepository
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.data.mongodb.core.query.isEqualTo
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
class CustomizedUserPlaylistRepositoryImpl(
    private val mongoTemplate: MongoTemplate
) : CustomizedUserPlaylistRepository {

    override fun update(playlistId: String, userPlaylist: UserPlaylist, userId: String): UserPlaylist {
        val update = Update()
                .set(UserPlaylist::title.name, userPlaylist.title)
                .set(UserPlaylist::privacy.name, userPlaylist.privacy)
                .set(UserPlaylist::topic.name, userPlaylist.topic)
                .set(UserPlaylist::image.name, userPlaylist.image)
                .set(UserPlaylist::lastModifiedDate.name, LocalDateTime.now())

        mongoTemplate.updateFirst(Query(UserPlaylist::id isEqualTo playlistId), update, UserPlaylist::class.java)

        val searchQuery = Query(Criteria().andOperator(
                UserPlaylist::id isEqualTo playlistId,
                UserPlaylist::userId isEqualTo userId)
        )
        return mongoTemplate.findOne(searchQuery, UserPlaylist::class.java)!!
    }

}