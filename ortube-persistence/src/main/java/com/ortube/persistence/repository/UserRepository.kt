package com.ortube.persistence.repository

import com.ortube.persistence.domain.User
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.Optional

interface UserRepository : MongoRepository<User, String>, CustomizedUserRepository {

    fun findByEmailOrUsername(email: String, username: String): Optional<User>

    fun findByEmail(email: String): Optional<User>

    fun findByUsername(username: String): Optional<User>

    fun findByActivationCode_Code(code: String): Optional<User>

}