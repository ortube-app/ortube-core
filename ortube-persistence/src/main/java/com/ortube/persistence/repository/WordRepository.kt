package com.ortube.persistence.repository

import com.ortube.persistence.domain.AbstractPlaylist
import com.ortube.persistence.domain.Word

interface WordRepository<E : AbstractPlaylist> {

    fun addWord(playlistId: String, word: Word, playlistClass: Class<E>): E

    fun update(playlistId: String, wordId: String, word: Word, playlistClass: Class<E>): E

    fun deleteByIdAndPlaylistId(wordId: String, playlistId: String, playlistClass: Class<E>)

    fun deleteAllByIdAndPlaylistId(wordIds: List<String>, playlistId: String, playlistClass: Class<E>)

}