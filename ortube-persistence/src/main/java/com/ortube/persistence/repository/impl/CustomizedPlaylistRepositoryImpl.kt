package com.ortube.persistence.repository.impl

import com.ortube.persistence.domain.Playlist
import com.ortube.persistence.repository.CustomizedPlaylistRepository
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.data.mongodb.core.query.isEqualTo
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
class CustomizedPlaylistRepositoryImpl(
    private val mongoTemplate: MongoTemplate
) : CustomizedPlaylistRepository {

    override fun update(id: String, playlist: Playlist): Playlist {
        val update = Update()
                .set(Playlist::title.name, playlist.title)
                .set(Playlist::topic.name, playlist.topic)
                .set(Playlist::image.name, playlist.image)
                .set(Playlist::lastModifiedDate.name, LocalDateTime.now())

        mongoTemplate.updateFirst(Query(Playlist::id isEqualTo id), update, Playlist::class.java)

        return mongoTemplate.findById(id, Playlist::class.java)!!
    }

}