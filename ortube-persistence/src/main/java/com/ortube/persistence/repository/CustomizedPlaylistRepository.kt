package com.ortube.persistence.repository

import com.ortube.persistence.domain.Playlist

interface CustomizedPlaylistRepository {

    fun update(id: String, playlist: Playlist): Playlist

}