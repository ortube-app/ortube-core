package com.ortube.persistence.repository.impl

import com.mongodb.BasicDBObject
import com.ortube.persistence.domain.AbstractPlaylist
import com.ortube.persistence.domain.Playlist
import com.ortube.persistence.domain.Word
import com.ortube.persistence.repository.WordRepository
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.data.mongodb.core.query.inValues
import org.springframework.data.mongodb.core.query.isEqualTo
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
class WordRepositoryImpl<E : AbstractPlaylist>(
    private val mongoTemplate: MongoTemplate
) : WordRepository<E> {

    override fun addWord(playlistId: String, word: Word, playlistClass: Class<E>): E {
        word.id = ObjectId.get().toHexString()

        val update = Update()
                .addToSet(AbstractPlaylist::words.name, word)
                .set(AbstractPlaylist::lastModifiedDate.name, LocalDateTime.now())

        mongoTemplate.updateFirst(Query(Playlist::id isEqualTo playlistId), update, playlistClass)

        return mongoTemplate.findById(playlistId, playlistClass)!!
    }

    override fun update(playlistId: String, wordId: String, word: Word, playlistClass: Class<E>): E {
        val playlist = mongoTemplate.findById(playlistId, playlistClass)!!
        val update = Update().set(AbstractPlaylist::lastModifiedDate.name, LocalDateTime.now())

        val words = playlist.words

        for (i in words.indices) {
            if (words[i].id == wordId) {
                update
                        .set("${AbstractPlaylist::words.name}.$i.${Word::lang1.name}", word.lang1)
                        .set("${AbstractPlaylist::words.name}.$i.${Word::lang2.name}", word.lang2)
                        .set("${AbstractPlaylist::words.name}.$i.${Word::image.name}", word.image)
            }
        }

        val query = Query(Playlist::id isEqualTo playlistId)
        mongoTemplate.updateFirst(query, update, playlistClass)

        return mongoTemplate.findById(playlistId, playlistClass)!!
    }

    override fun deleteByIdAndPlaylistId(wordId: String, playlistId: String, playlistClass: Class<E>) {
        val query = Query(Playlist::id isEqualTo playlistId)
        val update = Update().pull(AbstractPlaylist::words.name, BasicDBObject(Word::id.name, wordId))

        mongoTemplate.updateFirst(query, update, playlistClass)
    }

    override fun deleteAllByIdAndPlaylistId(wordIds: List<String>, playlistId: String, playlistClass: Class<E>) {
        val wordIdsToDelete = wordIds.map(::ObjectId)
        val findPlaylistQuery = Query(Playlist::id isEqualTo playlistId)
        val deleteQuery = Query(Word::id inValues wordIdsToDelete)

        val update = Update().pull(AbstractPlaylist::words.name, deleteQuery.queryObject)

        mongoTemplate.updateMulti(findPlaylistQuery, update, playlistClass)
    }

}