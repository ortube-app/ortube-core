package com.ortube.persistence.repository

import com.ortube.persistence.domain.UserPlaylist
import com.ortube.persistence.repository.cust.CustomizedPlaylistSave
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.Optional

interface UserPlaylistRepository : MongoRepository<UserPlaylist, String>,
    CustomizedUserPlaylistRepository,
    CustomizedPlaylistSave<UserPlaylist> {

    fun findAllByUserId(userId: String, sort: Sort): List<UserPlaylist>

    fun findByIdAndUserId(playlistId: String, userId: String): Optional<UserPlaylist>

}