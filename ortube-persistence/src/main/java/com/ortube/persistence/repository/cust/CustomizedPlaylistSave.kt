package com.ortube.persistence.repository.cust

interface CustomizedPlaylistSave<T> {
    fun <S : T> save(playlist: S): S
}