package com.ortube.persistence.repository.impl

import com.ortube.persistence.domain.User
import com.ortube.persistence.domain.UserPlaylist
import com.ortube.persistence.domain.enums.PlaylistPrivacy
import com.ortube.persistence.domain.projection.CommonUserProjection
import com.ortube.persistence.repository.CustomizedUserRepository
import org.bson.Document
import org.bson.types.ObjectId
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation
import org.springframework.data.mongodb.core.aggregation.AggregationOperation
import org.springframework.data.mongodb.core.aggregation.ArrayOperators.Filter.filter
import org.springframework.data.mongodb.core.aggregation.ComparisonOperators
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Repository
import java.util.Optional

@Repository
class CustomizedUserRepositoryImpl(
    private val mongoTemplate: MongoTemplate
) : CustomizedUserRepository {

    companion object {
        const val VALID_ID_LENGTH = 24
    }

    override fun findProjectionById(id: String): Optional<CommonUserProjection> {
        if (id.length < VALID_ID_LENGTH) {
            return Optional.empty()
        }

        val operations = operationsPlaylistCount()
        val match = Aggregation.match(where("_id").`is`(ObjectId(id)))

        val aggregation = Aggregation.newAggregation(*operations, match)

        val mappedResults = mongoTemplate.aggregate(aggregation, User::class.java, CommonUserProjection::class.java).mappedResults

        return if (mappedResults.size > 0) {
            Optional.of(mappedResults[0])
        } else {
            Optional.empty()
        }
    }

    override fun findAllCommonProjections(page: Int, size: Int): Page<CommonUserProjection> {
        val operations = operationsPlaylistCount()
        val skip = Aggregation.skip((page * size).toLong())
        val limit = Aggregation.limit(size.toLong())

        val aggregation = Aggregation.newAggregation(*operations, skip, limit)
        val results = mongoTemplate.aggregate(aggregation, User::class.java, CommonUserProjection::class.java)

        val count = mongoTemplate.count(Query(), User::class.java)
        return PageImpl(results.mappedResults, PageRequest.of(page, size), count)
    }

    private fun operationsPlaylistCount(): Array<AggregationOperation> {

        // need to convert _id to string, because _id is ObjectId by default
        // TODO: uncomment when spring boot will be upgraded to 2.3.x
        //        val addFieldId = Aggregation.addFields()
        //                .addField("userId")
        //                .withValue(ConvertOperators.ToString.toString("_id"))
        //                .build()
        val addFieldId = AggregationOperation {
            Document("\$addFields", Document(UserPlaylist::userId.name, Document("\$toString", "\$_id")))
        }

        val lookup = Aggregation.lookup("userPlaylist", UserPlaylist::userId.name, UserPlaylist::userId.name, "userPlaylists")

        val project = Aggregation.project(
                CommonUserProjection::image.name,
                CommonUserProjection::nickname.name,
                CommonUserProjection::username.name,
                "userPlaylists"
        )
                .and(filter("userPlaylists").`as`("userPlaylist")
                        .by(
                                ComparisonOperators.Eq
                                        .valueOf("userPlaylist.${UserPlaylist::privacy.name}")
                                        .equalToValue(PlaylistPrivacy.PUBLIC.name)
                        )
                ).`as`("userPlaylists")

        // TODO: uncomment when spring boot will be upgraded to 2.3.x
        //        val addFieldPlaylistCount = Aggregation.addFields()
        //                .addField("playlistCount")
        //                .withValue(ArrayOperators.Size.lengthOfArray("userPlaylists"))
        //                .build()
        val addFieldPlaylistCount = AggregationOperation {
            Document("\$addFields", Document(CommonUserProjection::playlistCount.name, Document("\$size", "\$userPlaylists")))
        }

        return arrayOf(addFieldId, lookup, project, addFieldPlaylistCount)
    }

}