package com.ortube.persistence.repository.cust

import com.ortube.persistence.domain.AbstractPlaylist
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Repository

@Repository
class CustomizedPlaylistSaveImpl(
    private val mongoTemplate: MongoTemplate
) : CustomizedPlaylistSave<AbstractPlaylist> {

    override fun <S : AbstractPlaylist> save(playlist: S): S {
        playlist.words.forEach { it.id = ObjectId.get().toHexString() }
        return mongoTemplate.save(playlist)
    }

}