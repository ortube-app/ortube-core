package com.ortube.persistence.domain

import com.ortube.persistence.domain.enums.UserRole
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class User(
    @Id
    var id: String? = null,

    @Indexed(unique = true)
    val email: String,

    @Indexed(unique = true)
    val username: String,

    var password: String,
    var userRole: UserRole = UserRole.USER,
    val nickname: String,
    var image: String? = null,
    var refreshTokens: List<RefreshToken> = mutableListOf(),
    var activationCode: ActivationCode? = null
)