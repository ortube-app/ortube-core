package com.ortube.persistence.domain

data class Word(
    var id: String? = null,
    val lang1: String,
    val lang2: String,
    var image: String? = null
) {

    object WordFields {
        const val WORD_ID = "id"
        const val LANG_1 = "lang1"
        const val LANG_2 = "lang2"
        const val IMAGE = "image"
    }
}