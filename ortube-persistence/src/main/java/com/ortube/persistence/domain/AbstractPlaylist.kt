package com.ortube.persistence.domain

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import java.time.LocalDateTime

abstract class AbstractPlaylist(
    val title: String,
    val topic: String? = null,
    val image: String? = null,
    var words: List<Word> = emptyList(),

    @CreatedDate
    var createdDate: LocalDateTime? = null,

    @LastModifiedDate
    var lastModifiedDate: LocalDateTime? = null
)