package com.ortube.persistence.domain.projection

data class CommonUserProjection(
    val id: String,
    val username: String,
    val nickname: String,
    val image: String?,
    val playlistCount: Int
)