package com.ortube.persistence.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
class Playlist(
    @Id
    var id: String? = null,
    title: String,
    topic: String? = null,
    image: String? = null,
    words: List<Word> = emptyList()
) : AbstractPlaylist(title, topic, image, words)