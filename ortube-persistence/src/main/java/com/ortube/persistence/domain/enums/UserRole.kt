package com.ortube.persistence.domain.enums

enum class UserRole {
    ADMIN,
    USER
}