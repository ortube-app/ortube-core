package com.ortube.persistence.domain

import com.ortube.persistence.domain.enums.PlaylistPrivacy
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
class UserPlaylist(
    @Id
    var id: String? = null,
    var privacy: PlaylistPrivacy = PlaylistPrivacy.PRIVATE,
    val userId: String,
    title: String,
    topic: String? = null,
    image: String? = null,
    words: List<Word> = emptyList()
) : AbstractPlaylist(title, topic, image, words) {

    fun copy(
        id: String? = this.id,
        privacy: PlaylistPrivacy = this.privacy,
        userId: String = this.userId,
        title: String = this.title,
        topic: String? = this.topic,
        image: String? = this.image,
        words: List<Word> = this.words
    ) = UserPlaylist(id, privacy, userId, title, topic, image, words)
}