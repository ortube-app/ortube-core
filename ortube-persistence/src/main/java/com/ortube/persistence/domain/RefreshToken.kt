package com.ortube.persistence.domain

import java.time.LocalDateTime

data class RefreshToken(
    val token: String,
    val browserFingerprint: String,
    val expiredAt: LocalDateTime? = null
) {
    constructor(token: String, browserFingerprint: String) : this(token, browserFingerprint, null)

    fun equalsExcludeExpiredAt(refreshToken: RefreshToken) =
            token == refreshToken.token && browserFingerprint == refreshToken.browserFingerprint
}