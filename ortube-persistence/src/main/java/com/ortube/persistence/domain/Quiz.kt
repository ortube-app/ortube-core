package com.ortube.persistence.domain

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document
data class Quiz(
    @Id
    var id: String? = null,
    val userId: String,
    val playlistId: String,
    var words: List<Word> = listOf(),
    val answers: MutableList<AnswerResult> = mutableListOf(),
    var finished: Boolean = false,
    var result: Int = 0,
    @CreatedDate
    var startDate: LocalDateTime? = null
) {

    fun addResult(answerResult: AnswerResult) {
        answers.add(answerResult)
    }
}