package com.ortube.service;

import com.ortube.dto.MockUserCreateDto;
import com.ortube.dto.UserAccountDto;
import com.ortube.dto.UserCreateDto;

public interface RegistrationService {

    UserAccountDto register(UserCreateDto userCreateDto);

    String createMockUser(MockUserCreateDto mockUserCreateDto);

    void activateAccount(String activationCode);

}
