package com.ortube.service;

import com.ortube.persistence.domain.UserPlaylist;

public interface UserPlaylistWordService extends WordService<UserPlaylist> {

}
