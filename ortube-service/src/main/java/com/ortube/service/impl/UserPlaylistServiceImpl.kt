package com.ortube.service.impl

import com.ortube.constants.PLAYLIST_NOT_FOUND_MSG
import com.ortube.constants.YOU_CANNOT_COPY_THIS_PLAYLIST_MSG
import com.ortube.dto.UserPlaylistBriefDto
import com.ortube.dto.UserPlaylistRequestDto
import com.ortube.exceptions.PlaylistNotFoundException
import com.ortube.persistence.domain.UserPlaylist
import com.ortube.persistence.domain.enums.PlaylistPrivacy
import com.ortube.persistence.repository.PlaylistRepository
import com.ortube.persistence.repository.UserPlaylistRepository
import com.ortube.security.model.UserContextHolder
import com.ortube.service.UserPlaylistService
import org.springframework.core.convert.ConversionService
import org.springframework.data.domain.Sort
import org.springframework.security.access.AccessDeniedException
import org.springframework.stereotype.Service

@Service
class UserPlaylistServiceImpl(
    private val userPlaylistRepository: UserPlaylistRepository,
    private val playlistRepository: PlaylistRepository,
    private val conversionService: ConversionService,
    private val userContextHolder: UserContextHolder
) : UserPlaylistService {

    override fun findAllSortedByLastModified(): List<UserPlaylistBriefDto> {
        val sort = Sort.by(Sort.Direction.DESC, UserPlaylist::lastModifiedDate.name)
        return userPlaylistRepository.findAllByUserId(userContextHolder.userId, sort)
                .map { conversionService.convert(it, UserPlaylistBriefDto::class.java)!! }
    }

    override fun findAllPublicSortedByLastModified(userId: String): List<UserPlaylistBriefDto> {
        val sort = Sort.by(Sort.Direction.DESC, UserPlaylist::lastModifiedDate.name)
        return userPlaylistRepository.findAllByUserId(userId, sort)
                .map { conversionService.convert(it, UserPlaylistBriefDto::class.java)!! }
                .filter { it.privacy == PlaylistPrivacy.PUBLIC }
    }

    override fun findById(userPlaylistId: String): UserPlaylist {
        return userPlaylistRepository.findByIdAndUserId(userPlaylistId, userContextHolder.userId)
                .orElseThrow { PlaylistNotFoundException(PLAYLIST_NOT_FOUND_MSG) }
    }

    override fun save(userPlaylistRequestDto: UserPlaylistRequestDto): UserPlaylist {
        val userPlaylist = conversionService.convert(userPlaylistRequestDto, UserPlaylist::class.java)!!

        return userPlaylistRepository.save(userPlaylist)
    }

    override fun addPlaylist(playlistId: String): UserPlaylist {
        val userPlaylist = playlistRepository.findById(playlistId)
                .orElseThrow { PlaylistNotFoundException(PLAYLIST_NOT_FOUND_MSG, playlistId) }
                .run { conversionService.convert(this, UserPlaylist::class.java)!! }

        return userPlaylistRepository.save(userPlaylist)
    }

    override fun addPlaylistFromUser(userIdCopyFrom: String, userPlaylistId: String): UserPlaylist {
        val userPlaylist = userPlaylistRepository.findByIdAndUserId(userPlaylistId, userIdCopyFrom)
                .orElseThrow { PlaylistNotFoundException(PLAYLIST_NOT_FOUND_MSG) }
                .apply {
                    if (this.privacy != PlaylistPrivacy.PUBLIC) {
                        throw AccessDeniedException(YOU_CANNOT_COPY_THIS_PLAYLIST_MSG)
                    }
                }
                .copy(id = null, privacy = PlaylistPrivacy.PRIVATE, userId = userContextHolder.userId)

        return userPlaylistRepository.save(userPlaylist)
    }

    override fun update(userPlaylistId: String, userPlaylistRequestDto: UserPlaylistRequestDto): UserPlaylist {
        val userId = userContextHolder.userId
        userPlaylistRepository.findByIdAndUserId(userPlaylistId, userId)
                .orElseThrow { PlaylistNotFoundException(PLAYLIST_NOT_FOUND_MSG) }

        val userPlaylist = conversionService.convert(userPlaylistRequestDto, UserPlaylist::class.java)!!
        return userPlaylistRepository.update(userPlaylistId, userPlaylist, userId)
    }

    override fun delete(userPlaylistId: String) {
        userPlaylistRepository.delete(findById(userPlaylistId))
    }
}