package com.ortube.service.impl;

import com.ortube.dto.WordRequestDto;
import com.ortube.exceptions.WordNotFoundException;
import com.ortube.persistence.domain.AbstractPlaylist;
import com.ortube.persistence.domain.Word;
import com.ortube.persistence.repository.WordRepository;
import com.ortube.service.WordService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ResolvableType;
import org.springframework.core.convert.ConversionService;

import java.util.List;

import static com.ortube.constants.ErrorMessagesKt.WORD_NOT_FOUND_MSG;

@RequiredArgsConstructor
public abstract class AbstractWordServiceImpl<T extends AbstractPlaylist> implements WordService<T> {

    protected final WordRepository<T> wordRepository;
    protected final ConversionService conversionService;
    @SuppressWarnings("unchecked")
    private final Class<T> playlistClass = (Class<T>) ResolvableType.forClass(getClass())
            .as(AbstractWordServiceImpl.class)
            .getGeneric(0)
            .toClass();

    public abstract AbstractPlaylist getPlaylistById(String playlistId);

    @Override
    public T addWordToPlaylist(String playlistId, WordRequestDto wordRequestDto) {
        getPlaylistById(playlistId);
        Word word = conversionService.convert(wordRequestDto, Word.class);

        return wordRepository.addWord(playlistId, word, playlistClass);
    }

    @Override
    public T update(String userPlaylistId, String wordId, WordRequestDto wordDto) {
        checkWordExistence(wordId, userPlaylistId);
        Word word = conversionService.convert(wordDto, Word.class);

        return wordRepository.update(userPlaylistId, wordId, word, playlistClass);
    }

    @Override
    public void deleteByIdAndPlaylistId(String wordId, String userPlaylistId) {
        checkWordExistence(wordId, userPlaylistId);
        wordRepository.deleteByIdAndPlaylistId(wordId, userPlaylistId, playlistClass);
    }

    @Override
    public void deleteAllByIdAndPlaylistId(List<String> wordIds, String playlistId) {
        List<Word> words = getPlaylistById(playlistId).getWords();
        wordIds.forEach(wordId -> checkWordExistence(words, wordId));

        wordRepository.deleteAllByIdAndPlaylistId(wordIds, playlistId, playlistClass);
    }

    protected void checkWordExistence(List<Word> words, String wordId) {
        words.stream()
                .map(Word::getId)
                .filter(wordId::equals)
                .findFirst()
                .orElseThrow(() -> new WordNotFoundException(WORD_NOT_FOUND_MSG, wordId));
    }

    protected void checkWordExistence(String wordId, String playlistId) {
        List<Word> words = getPlaylistById(playlistId).getWords();
        checkWordExistence(words, wordId);
    }

}
