package com.ortube.service.impl;

import com.ortube.persistence.domain.User;
import com.ortube.persistence.repository.UserRepository;
import com.ortube.security.model.JwtUser;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.core.convert.ConversionService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Primary
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    private final ConversionService conversionService;

    @Override
    public UserDetails loadUserByUsername(String emailOrUsername) {

        User user = userRepository.findByEmailOrUsername(emailOrUsername, emailOrUsername)
                .orElseThrow(() -> new UsernameNotFoundException("User not found: " + emailOrUsername));

        return conversionService.convert(user, JwtUser.class);
    }
}
