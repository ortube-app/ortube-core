package com.ortube.service.impl;

import com.ortube.dto.AuthenticationResponseDto;
import com.ortube.persistence.domain.RefreshToken;
import com.ortube.persistence.domain.User;
import com.ortube.persistence.repository.UserRepository;
import com.ortube.security.jwt.extractor.TokenExtractor;
import com.ortube.security.jwt.provider.token.JwtTokenProvider;
import com.ortube.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final JwtTokenProvider jwtTokenProvider;
    private final UserRepository userRepository;
    private final TokenExtractor tokenExtractor;

    @Override
    public AuthenticationResponseDto createTokens(User user, String browserFingerprint) {

        String token = jwtTokenProvider.createAccessToken(user.getUsername());
        LocalDateTime expiredAt = tokenExtractor.extractExpiredAt(token);
        String refreshToken = jwtTokenProvider.createRefreshToken(user.getUsername());

        String userRole = user.getUserRole().name();

        // TODO: introduce separate adding and removing tokens instead of saving the whole user
        RefreshToken refreshTokenToSave = new RefreshToken(
                refreshToken,
                browserFingerprint,
                tokenExtractor.extractExpiredAt(refreshToken)
        );

        // collect new list without refresh tokens that has current browserFingerprint
        List<RefreshToken> collect = user.getRefreshTokens()
                .stream()
                .filter(fingerprintNotEquals(browserFingerprint))
                .collect(toList());
        collect.add(refreshTokenToSave);

        user.setRefreshTokens(collect);
        userRepository.save(user);

        return AuthenticationResponseDto.builder()
                .role(userRole)
                .accessToken(token)
                .expiredAt(expiredAt)
                .refreshToken(refreshToken)
                .build();
    }

    @Override
    public AuthenticationResponseDto refreshTokens(User user, String token, String browserFingerprint) {

        // collect new list without refresh token from request
        List<RefreshToken> refreshTokens = user.getRefreshTokens()
                .stream()
                .filter(tokenNotEquals(token))
                .collect(toList());

        // TODO: introduce separate deleting tokens instead of saving the whole user
        user.setRefreshTokens(refreshTokens);
        userRepository.save(user);

        return createTokens(user, browserFingerprint);
    }

    private Predicate<RefreshToken> tokenNotEquals(String token) {
        return refreshToken -> !refreshToken.getToken().equals(token);
    }

    private Predicate<RefreshToken> fingerprintNotEquals(String fingerprint) {
        return refreshToken -> !refreshToken.getBrowserFingerprint().equals(fingerprint);
    }

}
