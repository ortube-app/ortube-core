package com.ortube.service.impl

import com.ortube.constants.INVALID_ACTIVATION_CODE_MSG
import com.ortube.constants.USER_NOT_FOUND_MSG
import com.ortube.dto.PageResult
import com.ortube.exceptions.InvalidActivationCodeException
import com.ortube.exceptions.UserNotFoundException
import com.ortube.persistence.domain.User
import com.ortube.persistence.domain.projection.CommonUserProjection
import com.ortube.persistence.repository.UserRepository
import com.ortube.service.UserService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserServiceImpl(
    private val userRepository: UserRepository
) : UserService {

    override fun findById(id: String): CommonUserProjection {
        return userRepository.findProjectionById(id).orElseThrow { UserNotFoundException() }
    }

    override fun findAll(page: Int, size: Int): PageResult<CommonUserProjection> {
        val userPages = userRepository.findAllCommonProjections(page, size)

        return PageResult(
                content = userPages.content,
                pageNumber = userPages.number,
                pageSize = size,
                totalPages = userPages.totalPages,
                first = userPages.isFirst,
                last = userPages.isLast
        )
    }

    override fun findByEmailOrUsername(emailOrUsername: String): User =
            userRepository.findByEmailOrUsername(emailOrUsername, emailOrUsername)
                    .orElseThrow { UsernameNotFoundException(USER_NOT_FOUND_MSG) }

    override fun save(user: User) = userRepository.save(user)

    override fun deleteActivationCode(activationCode: String): User {
        val user = userRepository.findByActivationCode_Code(activationCode)
                .orElseThrow { InvalidActivationCodeException(INVALID_ACTIVATION_CODE_MSG) }
                .apply { this.activationCode = null }

        return userRepository.save(user)
    }
}