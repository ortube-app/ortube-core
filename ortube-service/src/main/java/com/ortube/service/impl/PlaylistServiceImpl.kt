package com.ortube.service.impl

import com.ortube.constants.PLAYLIST_NOT_FOUND_MSG
import com.ortube.dto.PlaylistBriefDto
import com.ortube.dto.PlaylistRequestDto
import com.ortube.exceptions.PlaylistNotFoundException
import com.ortube.persistence.domain.Playlist
import com.ortube.persistence.repository.PlaylistRepository
import com.ortube.service.PlaylistService
import org.springframework.core.convert.ConversionService
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service

@Service
class PlaylistServiceImpl(
    private val playlistRepository: PlaylistRepository,
    private val conversionService: ConversionService
) : PlaylistService {

    override fun findAll(): List<PlaylistBriefDto> {
        val sort = Sort.by(Sort.Direction.DESC, Playlist::lastModifiedDate.name)
        return playlistRepository.findAll(sort)
                .map { conversionService.convert(it, PlaylistBriefDto::class.java)!! }
    }

    override fun findById(id: String): Playlist {
        return playlistRepository.findById(id)
                .orElseThrow { PlaylistNotFoundException(PLAYLIST_NOT_FOUND_MSG, id) }
    }

    override fun create(playlistRequestDto: PlaylistRequestDto): Playlist {
        val playlistToSave = conversionService.convert(playlistRequestDto, Playlist::class.java)!!
        return playlistRepository.save(playlistToSave)
    }

    override fun updatePlaylist(id: String, playlistRequestDto: PlaylistRequestDto): Playlist {
        findById(id)
        val playlistToUpdate = conversionService.convert(playlistRequestDto, Playlist::class.java)!!
        return playlistRepository.update(id, playlistToUpdate)
    }

    override fun deletePlaylist(id: String) {
        playlistRepository.delete(findById(id))
    }
}