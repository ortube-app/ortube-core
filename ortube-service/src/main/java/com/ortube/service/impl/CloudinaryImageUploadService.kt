package com.ortube.service.impl

import com.cloudinary.Cloudinary
import com.ortube.constants.FILE_IS_REQUIRED_MSG
import com.ortube.constants.enums.ImageExtension
import com.ortube.constants.enums.UploadFolderName
import com.ortube.dto.ImageUploadResponse
import com.ortube.exceptions.InvalidImageFormatException
import com.ortube.exceptions.MissingRequiredArgumentException
import com.ortube.service.ImageUploadService
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class CloudinaryImageUploadService(
    private val cloudinary: Cloudinary
) : ImageUploadService {

    override fun upload(folder: UploadFolderName, file: MultipartFile?): ImageUploadResponse {
        return when {
            (file == null) -> throw MissingRequiredArgumentException(FILE_IS_REQUIRED_MSG)
            (!ImageExtension.isValidExtension(file.getExtension())) -> throw InvalidImageFormatException()
            else -> {
                val upload = cloudinary.uploader().upload(file.bytes, mapOf("folder" to folder.name))
                ImageUploadResponse(upload["url"].toString(), folder, file.size)
            }
        }
    }

    private fun MultipartFile.getExtension() = originalFilename?.substringAfterLast(".")

}