package com.ortube.service

import com.ortube.constants.enums.ImageMode
import com.ortube.constants.enums.QuizMode
import com.ortube.dto.QuizDto
import com.ortube.dto.QuizResultDto
import com.ortube.persistence.domain.AnswerResult

interface QuizService {

    fun startQuiz(userPlaylistId: String, mode: QuizMode, image: ImageMode): QuizDto

    fun getOptions(quizId: String, wordTitle: String): List<String>

    fun giveAnAnswer(quizId: String, wordTitle: String, userAnswers: List<String>?): AnswerResult

    fun finishQuiz(quizId: String): QuizResultDto

}