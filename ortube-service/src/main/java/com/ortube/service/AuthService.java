package com.ortube.service;

import com.ortube.dto.AuthenticationResponseDto;
import com.ortube.persistence.domain.User;

public interface AuthService {

    AuthenticationResponseDto createTokens(User user, String browserFingerprint);

    AuthenticationResponseDto refreshTokens(User user, String token, String browserFingerprint);

}
