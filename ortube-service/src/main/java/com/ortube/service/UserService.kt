package com.ortube.service

import com.ortube.dto.PageResult
import com.ortube.persistence.domain.User
import com.ortube.persistence.domain.projection.CommonUserProjection

interface UserService {

    fun findById(id: String): CommonUserProjection

    fun findAll(page: Int, size: Int): PageResult<CommonUserProjection>

    fun findByEmailOrUsername(emailOrUsername: String): User

    fun save(user: User): User

    fun deleteActivationCode(activationCode: String): User
}