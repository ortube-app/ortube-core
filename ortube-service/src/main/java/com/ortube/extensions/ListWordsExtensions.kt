package com.ortube.extensions

import com.ortube.constants.CORRECT_OPTIONS_NOT_FOUND_MSG
import com.ortube.constants.enums.ImageMode
import com.ortube.constants.enums.QuizMode
import com.ortube.dto.QuizOptionDto
import com.ortube.exceptions.WordNotFoundException
import com.ortube.persistence.domain.Word

const val MIN_ALLOWED_WORDS_COUNT = 2
const val MIN_OPTIONS_COUNT = 4
const val OPTIONS_TO_DILUTE_COUNT = 2

fun List<Word>.getOptionsToStartQuiz(mode: QuizMode, image: ImageMode): List<QuizOptionDto> {

    val imageFun: (word: Word) -> String? = {
        when (image) {
            ImageMode.WITH_IMAGE -> it.image
            ImageMode.NO_IMAGE -> null
        }
    }

    return when (mode) {
        QuizMode.LANG1_LANG2 -> this.map { QuizOptionDto(word = it.lang1, image = imageFun(it)) }

        QuizMode.LANG2_LANG1 -> this.map { QuizOptionDto(word = it.lang2, image = imageFun(it)) }

        QuizMode.MIXED -> {
            val halfSize = size / 2
            this.shuffled()
                    .take(halfSize)
                    .map { QuizOptionDto(word = it.lang1, image = imageFun(it)) }
                    .plus(
                            this.takeLast(size - halfSize)
                                    .map { QuizOptionDto(word = it.lang2, image = imageFun(it)) }
                    )
        }
    }.distinct().shuffled()
}

fun List<Word>.getIncorrectOptions(wordTitle: String, correctOptionsSize: Int): List<String> {
    // calculate how many wrong options need to add
    val incorrectOptionsSize =
            if (correctOptionsSize < MIN_OPTIONS_COUNT) MIN_OPTIONS_COUNT - correctOptionsSize
            else OPTIONS_TO_DILUTE_COUNT // if we have more than 4 words, add 2 more just to dilute

    var rightWords = this.filter(rightWord(wordTitle) { it.lang1 })

    val options = if (rightWords.isNotEmpty()) {
        this.filterNot { rightWords.any { word -> word.lang2.equalsIgnoringNonLetters(it.lang2) } }.map { it.lang2 }
    } else {
        rightWords = this.filter(rightWord(wordTitle) { it.lang2 })
        this.filterNot { rightWords.any { word -> word.lang1.equalsIgnoringNonLetters(it.lang1) } }.map { it.lang1 }
    }

    return options
            .shuffled()
            .distinct()
            .take(incorrectOptionsSize)
}

fun List<Word>.getCorrectOptions(wordTitle: String): MutableList<String> {

    var options = this
            .filter(rightWord(wordTitle) { it.lang1 })
            .map { it.lang2 }

    if (options.isEmpty()) {
        options = this
                .filter(rightWord(wordTitle) { it.lang2 })
                .map { it.lang1 }
    }

    return options
            .ifEmpty { throw WordNotFoundException(CORRECT_OPTIONS_NOT_FOUND_MSG) }
            .toMutableList()
}

fun rightWord(wordTitle: String, getWordTitle: (word: Word) -> String) = { word: Word ->
    getWordTitle(word).equalsIgnoringNonLetters(wordTitle)
}