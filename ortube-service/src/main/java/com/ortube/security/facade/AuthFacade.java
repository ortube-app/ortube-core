package com.ortube.security.facade;

import com.ortube.dto.AuthenticationResponseDto;
import com.ortube.dto.LoginRequestDto;
import com.ortube.dto.UserAccountDto;

public interface AuthFacade {

    AuthenticationResponseDto authenticate(LoginRequestDto loginRequestDto);

    AuthenticationResponseDto refreshAccessToken();

    UserAccountDto getMe();

}
