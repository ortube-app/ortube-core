package com.ortube.security.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

data class JwtUser(
    val id: String,
    val userName: String,
    val nickname: String,
    val pass: String,
    val email: String,
    var image: String? = null,
    val enabled: Boolean = false,
    val listAuthorities: Collection<GrantedAuthority>
) : UserDetails {

    override fun getAuthorities() = listAuthorities

    override fun getPassword() = pass

    override fun getUsername() = userName

    override fun isEnabled() = enabled

    @JsonIgnore
    override fun isAccountNonExpired() = true

    @JsonIgnore
    override fun isAccountNonLocked() = true

    @JsonIgnore
    override fun isCredentialsNonExpired() = true
}