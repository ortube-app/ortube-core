package com.ortube.security.jwt.validator;

import com.ortube.persistence.domain.RefreshToken;
import com.ortube.persistence.domain.User;
import com.ortube.security.jwt.extractor.TokenExtractor;
import com.ortube.security.model.TokenScopes;
import com.ortube.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.ortube.constants.HeadersKt.AUTHENTICATION_HEADER;
import static com.ortube.constants.HeadersKt.BROWSER_FINGERPRINT_HEADER;

@Component
@TargetToken(TokenScopes.REFRESH_TOKEN)
public class RefreshTokenValidator extends AbstractTokenValidator {

    private final UserService userService;

    @Autowired
    public RefreshTokenValidator(UserService userService,
                                 TokenExtractor tokenExtractor) {
        super(tokenExtractor);
        this.userService = userService;
    }

    @Override
    public boolean validateToken(Map<String, String> headers) {
        String refreshToken = headers.get(AUTHENTICATION_HEADER);
        String browserFingerprint = headers.get(BROWSER_FINGERPRINT_HEADER);

        String username = tokenExtractor.extractSubject(refreshToken);
        User user = userService.findByEmailOrUsername(username);

        return isNotExpired(refreshToken) && isUserOwnsToken(refreshToken, browserFingerprint, user);
    }

    private boolean isUserOwnsToken(String token, String browserFingerprint, User user) {
        RefreshToken refreshToken = new RefreshToken(token, browserFingerprint);
        return user.getRefreshTokens()
                .stream()
                .anyMatch(refreshToken::equalsExcludeExpiredAt);
    }

}
