package com.ortube.security.jwt.validator;

import com.ortube.security.jwt.extractor.TokenExtractor;

import java.time.LocalDateTime;

public abstract class AbstractTokenValidator implements TokenValidator {

    protected final TokenExtractor tokenExtractor;

    public AbstractTokenValidator(TokenExtractor tokenExtractor) {
        this.tokenExtractor = tokenExtractor;
    }

    protected boolean isNotExpired(String token) {
        return !tokenExtractor.extractExpiredAt(token).isBefore(LocalDateTime.now());
    }

}
