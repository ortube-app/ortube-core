package com.ortube.security.jwt.extractor;

import com.ortube.constants.HeadersKt;
import com.ortube.exceptions.JwtAuthenticationException;
import com.ortube.security.jwt.JwtUtils;
import com.ortube.security.model.TokenScopes;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static com.ortube.constants.ErrorMessagesKt.INVALID_TOKEN_MSG;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Component
public class TokenExtractorImpl implements TokenExtractor {

    @Value("${jwt.token.secret}")
    protected String tokenSigningKey;

    @Override
    public String extractToken(String header) {
        String token = null;
        if (isNotBlank(header)) {
            token = header.substring(HeadersKt.HEADER_PREFIX.length());
        }
        return token;
    }

    @Override
    public String extractSubject(String token) {
        return getClaimsBody(token).getSubject();
    }

    @Override
    public TokenScopes extractScope(String token) {
        return TokenScopes.fromString((String) getClaimsBody(token).get("scope"));
    }

    @Override
    public LocalDateTime extractExpiredAt(String token) {
        return LocalDateTime.ofInstant(getClaimsBody(token)
                .getExpiration()
                .toInstant(), ZoneId.systemDefault());
    }

    private Claims getClaimsBody(String token) {
        try {
            return JwtUtils.parse(tokenSigningKey, token);
        } catch (JwtException | IllegalArgumentException e) {
            throw new JwtAuthenticationException(INVALID_TOKEN_MSG, e);
        }
    }

}
