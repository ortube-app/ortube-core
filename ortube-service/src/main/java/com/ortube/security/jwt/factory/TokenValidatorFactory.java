package com.ortube.security.jwt.factory;

import com.ortube.security.jwt.validator.TargetToken;
import com.ortube.security.jwt.validator.TokenValidator;
import com.ortube.security.model.TokenScopes;
import lombok.RequiredArgsConstructor;
import org.springframework.aop.framework.AopProxyUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class TokenValidatorFactory {

    private final List<TokenValidator> tokenValidators;

    private Map<TokenScopes, TokenValidator> validatorMap = new HashMap<>();

    @PostConstruct
    @SuppressWarnings("PMD.UnusedPrivateMethod")
    private void init() {
        for (TokenValidator tokenValidator : tokenValidators) {
            TargetToken annotation = AopProxyUtils.ultimateTargetClass(tokenValidator).getAnnotation(TargetToken.class);
            validatorMap.put(annotation.value(), tokenValidator);
        }
    }

    public TokenValidator getTokenValidator(TokenScopes tokenScope) {
        return validatorMap.get(tokenScope);
    }

}
