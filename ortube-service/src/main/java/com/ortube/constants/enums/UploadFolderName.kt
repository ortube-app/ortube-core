package com.ortube.constants.enums

enum class UploadFolderName {
    playlists,
    words,
    users
}