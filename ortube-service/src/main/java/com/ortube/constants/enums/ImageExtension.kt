package com.ortube.constants.enums

enum class ImageExtension {
    JPG,
    JPEG,
    PNG;

    companion object {
        fun isValidExtension(extension: String?) = values().any { it.name.equals(extension, ignoreCase = true) }
    }

}