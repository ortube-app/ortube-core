package com.ortube.exceptions

class WordNotFoundException : EntityNotFoundException {
    constructor(message: String) : super(message)
    constructor(message: String, criteria: String) : super(message, criteria)
}