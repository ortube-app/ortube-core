package com.ortube.exceptions

import com.ortube.constants.QUIZ_NOT_FOUND_MSG

class QuizNotFoundException : EntityNotFoundException(QUIZ_NOT_FOUND_MSG)