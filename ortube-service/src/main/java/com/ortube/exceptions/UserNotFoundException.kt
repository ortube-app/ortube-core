package com.ortube.exceptions

import com.ortube.constants.USER_NOT_FOUND_MSG

class UserNotFoundException : EntityNotFoundException(USER_NOT_FOUND_MSG)