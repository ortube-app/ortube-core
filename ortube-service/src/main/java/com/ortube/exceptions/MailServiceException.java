package com.ortube.exceptions;

public class MailServiceException extends RuntimeException {
    public MailServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
