package com.ortube.exceptions;

import lombok.Getter;

public class EntityNotFoundException extends RuntimeException {

    @Getter
    private String details;

    public EntityNotFoundException(String message, String criteria) {
        super(message);
        this.details = message + " Criteria: " + criteria;
    }

    public EntityNotFoundException(String message) {
        super(message);
    }

}
