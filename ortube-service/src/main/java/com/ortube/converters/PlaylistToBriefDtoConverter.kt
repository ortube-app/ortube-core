package com.ortube.converters

import com.ortube.constants.PLAYLIST_WORD_PREVIEW
import com.ortube.dto.PlaylistBriefDto
import com.ortube.persistence.domain.Playlist
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class PlaylistToBriefDtoConverter(
    private val wordToBriefDtoConverter: WordToBriefDtoConverter
) : Converter<Playlist, PlaylistBriefDto> {
    override fun convert(source: Playlist) = PlaylistBriefDto(
            id = source.id!!,
            title = source.title,
            topic = source.topic,
            wordCount = source.words.size,
            words = source.words.take(PLAYLIST_WORD_PREVIEW).map { wordToBriefDtoConverter.convert(it)!! },
            image = source.image
    )
}