package com.ortube.converters;

import com.ortube.dto.UserPlaylistRequestDto;
import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.security.model.UserContextHolder;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
@RequiredArgsConstructor
public class RequestDtoToUserPlaylistConverter implements Converter<UserPlaylistRequestDto, UserPlaylist> {

    private final UserContextHolder userContextHolder;

    @Override
    public UserPlaylist convert(UserPlaylistRequestDto userPlaylistRequestDto) {
        return new UserPlaylist(
                null,
                userPlaylistRequestDto.getPrivacy(),
                userContextHolder.getUserId(),
                userPlaylistRequestDto.getTitle(),
                userPlaylistRequestDto.getTopic(),
                userPlaylistRequestDto.getImage(),
                Collections.emptyList()
        );
    }
}
