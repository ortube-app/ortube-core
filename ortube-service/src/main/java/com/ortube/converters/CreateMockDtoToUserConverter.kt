package com.ortube.converters

import com.ortube.dto.MockUserCreateDto
import com.ortube.persistence.domain.User
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class CreateMockDtoToUserConverter : Converter<MockUserCreateDto, User> {
    override fun convert(source: MockUserCreateDto) = User(
            email = source.email!!,
            nickname = source.nickName!!,
            password = source.password!!,
            username = source.userName!!,
            userRole = source.role!!,
            image = source.image
    )
}