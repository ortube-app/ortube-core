package com.ortube.converters

import com.ortube.constants.PLAYLIST_WORD_PREVIEW
import com.ortube.dto.UserPlaylistBriefDto
import com.ortube.persistence.domain.UserPlaylist
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class UserPlaylistToBriefDtoConverter(
    private val wordToBriefDtoConverter: WordToBriefDtoConverter
) : Converter<UserPlaylist, UserPlaylistBriefDto> {

    override fun convert(userPlaylist: UserPlaylist) =
            UserPlaylistBriefDto(
                    id = userPlaylist.id!!,
                    topic = userPlaylist.topic,
                    title = userPlaylist.title,
                    privacy = userPlaylist.privacy,
                    wordCount = userPlaylist.words.size,
                    words = userPlaylist.words.take(PLAYLIST_WORD_PREVIEW).map { wordToBriefDtoConverter.convert(it)!! },
                    image = userPlaylist.image
            )

}