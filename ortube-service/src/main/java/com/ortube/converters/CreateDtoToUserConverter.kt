package com.ortube.converters

import com.ortube.dto.UserCreateDto
import com.ortube.persistence.domain.User
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class CreateDtoToUserConverter : Converter<UserCreateDto, User> {
    override fun convert(source: UserCreateDto) = User(
            email = source.email!!,
            username = source.username!!,
            password = source.password!!,
            nickname = source.nickname!!
    )
}