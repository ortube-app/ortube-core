package com.ortube.converters

import com.ortube.persistence.domain.Playlist
import com.ortube.persistence.domain.UserPlaylist
import com.ortube.security.model.UserContextHolder
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class PlaylistToUserPlaylistConverter(
    private val userContextHolder: UserContextHolder
) : Converter<Playlist, UserPlaylist> {
    override fun convert(playlist: Playlist) = UserPlaylist(
            title = playlist.title,
            topic = playlist.topic,
            words = playlist.words,
            image = playlist.image,
            userId = userContextHolder.userId
    )
}