package com.ortube.converters

import com.ortube.persistence.domain.User
import com.ortube.security.model.JwtUser
import org.springframework.core.convert.converter.Converter
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component

@Component
class UserToJwtUserDtoConverter : Converter<User, JwtUser> {
    override fun convert(source: User) = JwtUser(
            id = source.id!!,
            email = source.email,
            userName = source.username,
            pass = source.password,
            nickname = source.nickname,
            enabled = source.activationCode == null, // if activation code is present user is disabled
            listAuthorities = listOf(SimpleGrantedAuthority(source.userRole.name)),
            image = source.image
    )
}