package com.ortube.converters

import com.ortube.dto.PlaylistRequestDto
import com.ortube.persistence.domain.Playlist
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class RequestDtoToPlaylistConverter : Converter<PlaylistRequestDto, Playlist> {
    override fun convert(source: PlaylistRequestDto) = Playlist(
            title = source.title!!,
            topic = source.topic,
            image = source.image
    )
}