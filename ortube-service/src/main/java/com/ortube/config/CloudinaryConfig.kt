package com.ortube.config

import com.cloudinary.Cloudinary
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class CloudinaryConfig(
    private val cloudinaryProperties: CloudinaryProperties
) {

    @Bean
    fun cloudinary() = Cloudinary(mapOf(
            "cloud_name" to cloudinaryProperties.cloudname,
            "api_key" to cloudinaryProperties.apikey,
            "api_secret" to cloudinaryProperties.apisecret
    ))

    @Configuration
    @ConfigurationProperties("cloudinary")
    class CloudinaryProperties(
        var cloudname: String = "",
        var apikey: String = "",
        var apisecret: String = ""
    )

}