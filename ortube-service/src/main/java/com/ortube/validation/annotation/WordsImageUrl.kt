package com.ortube.validation.annotation

import com.ortube.constants.INVALID_IMAGE_URL
import com.ortube.validation.validator.WordsImageUrlValidator
import javax.validation.Constraint
import kotlin.reflect.KClass

@MustBeDocumented
@Constraint(validatedBy = [WordsImageUrlValidator::class])
@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY_GETTER)
@Retention(AnnotationRetention.RUNTIME)
annotation class WordsImageUrl(

    val message: String = INVALID_IMAGE_URL,

    val groups: Array<KClass<out Any>> = [],

    val payload: Array<KClass<out Any>> = []

)