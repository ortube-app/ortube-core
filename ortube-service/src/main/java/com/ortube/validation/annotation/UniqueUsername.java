package com.ortube.validation.annotation;

import com.ortube.validation.validator.UniqueUsernameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static com.ortube.constants.ErrorMessagesKt.USERNAME_IS_TAKEN_MSG;

@Documented
@Constraint(validatedBy = UniqueUsernameValidator.class)
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueUsername {

    String message() default USERNAME_IS_TAKEN_MSG;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
