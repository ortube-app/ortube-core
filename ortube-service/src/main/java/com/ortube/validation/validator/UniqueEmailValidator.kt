package com.ortube.validation.validator

import com.ortube.persistence.repository.UserRepository
import com.ortube.validation.annotation.UniqueEmail
import org.springframework.stereotype.Component
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

@Component
class UniqueEmailValidator(
    private val userRepository: UserRepository
) : ConstraintValidator<UniqueEmail, String?> {
    override fun isValid(email: String?, validatorContext: ConstraintValidatorContext?): Boolean {
        return if (email.isNullOrBlank()) {
            false
        } else userRepository.findByEmail(email).isEmpty
    }
}