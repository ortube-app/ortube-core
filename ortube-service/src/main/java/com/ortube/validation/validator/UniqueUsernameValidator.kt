package com.ortube.validation.validator

import com.ortube.persistence.repository.UserRepository
import com.ortube.validation.annotation.UniqueUsername
import org.springframework.stereotype.Component
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

@Component
class UniqueUsernameValidator(
    private val userRepository: UserRepository
) : ConstraintValidator<UniqueUsername, String?> {
    override fun isValid(username: String?, validatorContext: ConstraintValidatorContext?): Boolean {
        return if (username.isNullOrBlank()) {
            false
        } else userRepository.findByUsername(username).isEmpty
    }
}