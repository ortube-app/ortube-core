package com.ortube.dto;

import com.ortube.persistence.domain.enums.PlaylistPrivacy;
import com.ortube.validation.annotation.PlaylistsImageUrl;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class UserPlaylistRequestDto {

    @NotEmpty
    private String title;

    private String topic;

    @PlaylistsImageUrl
    private String image;

    @NotNull
    private PlaylistPrivacy privacy;

}
