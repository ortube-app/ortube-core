package com.ortube.dto

import com.ortube.validation.annotation.Password
import com.ortube.validation.annotation.UniqueEmail
import com.ortube.validation.annotation.UniqueUsername
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty

data class UserCreateDto(
    @get:Email
    @get:NotEmpty
    @get:UniqueEmail
    var email: String? = null,

    @get:NotEmpty
    @get:Password
    var password: String? = null,

    @get:UniqueUsername
    @get:NotEmpty
    var username: String? = null,

    @get:NotEmpty
    var nickname: String? = null
)