package com.ortube.dto

data class QuizDto(
    val quizId: String,
    val words: List<QuizOptionDto>
)