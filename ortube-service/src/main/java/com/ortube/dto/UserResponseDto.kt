package com.ortube.dto

data class UserResponseDto(
    val id: String,
    val username: String,
    val nickname: String,
    val image: String?,
    val playlistCount: Int
)