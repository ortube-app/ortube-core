package com.ortube.dto

import com.ortube.constants.enums.UploadFolderName

data class ImageUploadResponse(
    val imageLink: String,
    val folder: UploadFolderName,
    val size: Long
)