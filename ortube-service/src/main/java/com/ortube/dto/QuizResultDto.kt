package com.ortube.dto

import com.ortube.persistence.domain.AnswerResult

data class QuizResultDto(
    val result: Int,
    val answers: List<AnswerResult>
)