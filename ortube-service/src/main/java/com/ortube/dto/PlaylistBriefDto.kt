package com.ortube.dto

class PlaylistBriefDto(
    val id: String,
    val title: String,
    val topic: String?,
    val image: String?,
    val wordCount: Int,
    val words: List<WordBriefDto>
)