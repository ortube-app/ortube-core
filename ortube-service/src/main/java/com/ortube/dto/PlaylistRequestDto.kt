package com.ortube.dto

import com.ortube.validation.annotation.PlaylistsImageUrl
import javax.validation.constraints.NotEmpty

class PlaylistRequestDto(
    @get:NotEmpty
    val title: String?,
    val topic: String? = null,
    @get:PlaylistsImageUrl
    val image: String? = null
)