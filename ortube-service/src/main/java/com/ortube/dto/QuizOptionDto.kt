package com.ortube.dto

data class QuizOptionDto(
    val word: String? = null,
    val image: String? = null
)