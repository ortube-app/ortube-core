package com.ortube.dto

import com.ortube.persistence.domain.enums.UserRole
import com.ortube.validation.annotation.UniqueEmail
import com.ortube.validation.annotation.UniqueUsername
import com.ortube.validation.annotation.UsersImageUrl
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

data class MockUserCreateDto(

    @get:UniqueEmail
    @get:NotBlank
    var email: String? = null,

    @get:UniqueUsername
    @get:NotBlank
    var userName: String? = null,

    @get:UsersImageUrl
    var image: String? = null,

    @get:NotBlank
    var password: String? = null,

    @get:NotNull
    var role: UserRole? = null,

    @get:NotBlank
    var nickName: String? = null

)