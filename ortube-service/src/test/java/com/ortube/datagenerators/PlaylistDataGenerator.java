package com.ortube.datagenerators;

import com.ortube.persistence.domain.Playlist;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;

@TestComponent
@Profile("integration-test")
@RequiredArgsConstructor
public class PlaylistDataGenerator {

    private final MockPlaylistDataGenerator mockPlaylistDataGenerator;
    private final MongoTemplate mongoTemplate;

    public Playlist playlist() {
        Playlist playlist = mockPlaylistDataGenerator.playlist();
        playlist.getWords().forEach(word -> word.setId(ObjectId.get().toHexString()));
        return mongoTemplate.save(playlist);
    }

}
