package com.ortube.datagenerators

import com.ortube.persistence.domain.Playlist
import org.apache.commons.lang3.RandomStringUtils
import org.bson.types.ObjectId
import org.springframework.boot.test.context.TestComponent
import org.springframework.context.annotation.Profile

@TestComponent
@Profile("integration-test")
class MockPlaylistDataGenerator {

    private val wordDataGenerator = WordDataGenerator()

    fun playlist() = Playlist(
            id = ObjectId.get().toHexString(),
            title = RandomStringUtils.randomAlphabetic(30),
            topic = RandomStringUtils.randomAlphabetic(30),
            words = wordDataGenerator.words()
    )

}