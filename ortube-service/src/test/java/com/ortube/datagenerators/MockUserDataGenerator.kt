package com.ortube.datagenerators

import com.ortube.dto.MockUserCreateDto
import com.ortube.dto.UserCreateDto
import com.ortube.persistence.domain.User
import com.ortube.persistence.domain.enums.UserRole
import com.ortube.persistence.domain.projection.CommonUserProjection
import com.ortube.security.model.JwtUser
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.apache.commons.lang3.RandomUtils
import org.bson.types.ObjectId
import org.springframework.boot.test.context.TestComponent
import org.springframework.context.annotation.Profile
import org.springframework.security.core.authority.SimpleGrantedAuthority

@TestComponent
@Profile("integration-test")
class MockUserDataGenerator {

    fun user() = User(
            id = ObjectId.get().toHexString(),
            email = randomAlphabetic(20) + "@gmail.com",
            password = randomAlphabetic(20),
            nickname = randomAlphabetic(20),
            username = randomAlphabetic(20),
            userRole = UserRole.USER
    )

    fun userWithoutId() = User(
            email = randomAlphabetic(20),
            password = randomAlphabetic(20),
            nickname = randomAlphabetic(20),
            username = randomAlphabetic(20),
            userRole = UserRole.USER
    )

    fun jwtUserDto() = JwtUser(
            id = ObjectId.get().toHexString(),
            email = randomAlphabetic(20),
            pass = randomAlphabetic(20),
            nickname = randomAlphabetic(20),
            userName = randomAlphabetic(20),
            listAuthorities = listOf(SimpleGrantedAuthority(UserRole.USER.name))
    )

    fun mockUserCreateDto() = MockUserCreateDto(
            email = randomAlphabetic(20),
            password = randomAlphabetic(20),
            nickName = randomAlphabetic(20),
            userName = randomAlphabetic(20),
            role = UserRole.USER
    )

    fun userCreateDto() = UserCreateDto(
            email = randomAlphabetic(20),
            password = randomAlphabetic(20),
            nickname = randomAlphabetic(20),
            username = randomAlphabetic(20)
    )

    fun commonUserProjection() = CommonUserProjection(
            id = ObjectId.get().toHexString(),
            nickname = randomAlphabetic(20),
            username = randomAlphabetic(20),
            image = randomAlphabetic(20),
            playlistCount = RandomUtils.nextInt()
    )

}