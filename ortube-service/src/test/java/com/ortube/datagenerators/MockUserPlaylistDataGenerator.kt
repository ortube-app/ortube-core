package com.ortube.datagenerators

import com.ortube.dto.UserPlaylistRequestDto
import com.ortube.persistence.domain.UserPlaylist
import com.ortube.persistence.domain.Word
import com.ortube.persistence.domain.enums.PlaylistPrivacy
import org.apache.commons.lang3.RandomStringUtils
import org.bson.types.ObjectId
import org.springframework.boot.test.context.TestComponent
import org.springframework.context.annotation.Profile

@TestComponent
@Profile("integration-test")
class MockUserPlaylistDataGenerator {

    private val wordDataGenerator = WordDataGenerator()

    fun userPlaylist(
        playlistId: String? = ObjectId.get().toHexString(),
        title: String = RandomStringUtils.randomAlphabetic(20),
        topic: String? = RandomStringUtils.randomAlphabetic(20),
        privacy: PlaylistPrivacy = PlaylistPrivacy.PRIVATE,
        userId: String = ObjectId.get().toHexString(),
        image: String? = RandomStringUtils.randomAlphabetic(20),
        words: List<Word> = wordDataGenerator.words(5)
    ) = UserPlaylist(
            id = playlistId,
            title = title,
            topic = topic,
            privacy = privacy,
            userId = userId,
            image = image,
            words = words.toMutableList()
    )

    fun userPlaylistRequestDto(): UserPlaylistRequestDto {
        return UserPlaylistRequestDto.builder()
                .title(RandomStringUtils.randomAlphabetic(20))
                .topic(RandomStringUtils.randomAlphabetic(20))
                .privacy(PlaylistPrivacy.PRIVATE)
                .build()
    }
}