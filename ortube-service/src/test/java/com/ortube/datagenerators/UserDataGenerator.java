package com.ortube.datagenerators;

import com.ortube.persistence.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;

@TestComponent
@Profile("integration-test")
@RequiredArgsConstructor
public class UserDataGenerator {

    private final MongoTemplate mongoTemplate;
    private final MockUserDataGenerator mockUserDataGenerator;

    public User user() {
        User user = mockUserDataGenerator.user();
        return mongoTemplate.save(user);
    }
}
