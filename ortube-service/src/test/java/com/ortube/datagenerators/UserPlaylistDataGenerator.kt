package com.ortube.datagenerators

import com.ortube.persistence.domain.UserPlaylist
import com.ortube.persistence.domain.enums.PlaylistPrivacy
import org.bson.types.ObjectId
import org.springframework.boot.test.context.TestComponent
import org.springframework.context.annotation.Profile
import org.springframework.data.mongodb.core.MongoTemplate

@TestComponent
@Profile("integration-test")
class UserPlaylistDataGenerator(
    private val mongoTemplate: MongoTemplate,
    private val mockUserPlaylistDataGenerator: MockUserPlaylistDataGenerator
) {

    @JvmOverloads
    fun userPlaylist(userId: String, privacy: PlaylistPrivacy = PlaylistPrivacy.PUBLIC): UserPlaylist {
        val userPlaylist = mockUserPlaylistDataGenerator.userPlaylist(userId = userId, privacy = privacy)
                .apply { words.forEach { it.id = ObjectId.get().toHexString() } }

        return mongoTemplate.save(userPlaylist)
    }

    fun userPlaylists(userId: String, size: Int): List<UserPlaylist> {
        return generateSequence { userPlaylist(userId, PlaylistPrivacy.PUBLIC) }.take(size).toList()
    }

}