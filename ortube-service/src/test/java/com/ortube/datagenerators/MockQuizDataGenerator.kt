package com.ortube.datagenerators

import com.ortube.persistence.domain.Quiz
import com.ortube.persistence.domain.Word
import java.time.LocalDateTime

class MockQuizDataGenerator {

    fun quiz(
        quizId: String,
        userId: String,
        playlistId: String,
        words: MutableList<Word>
    ) = Quiz(
            id = quizId,
            userId = userId,
            playlistId = playlistId,
            words = ArrayList(words),
            startDate = LocalDateTime.now()
    )

}