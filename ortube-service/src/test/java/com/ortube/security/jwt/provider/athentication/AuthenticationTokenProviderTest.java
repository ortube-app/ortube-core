package com.ortube.security.jwt.provider.athentication;

import com.ortube.config.AbstractUnitTest;
import com.ortube.datagenerators.MockUserDataGenerator;
import com.ortube.security.jwt.extractor.TokenExtractor;
import com.ortube.security.jwt.provider.authentication.AuthenticationTokenProviderImpl;
import com.ortube.security.model.JwtUser;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class AuthenticationTokenProviderTest extends AbstractUnitTest {

    @Mock
    private UserDetailsService userDetailsService;

    @Mock
    private TokenExtractor extractor;

    @InjectMocks
    private AuthenticationTokenProviderImpl authenticationTokenProvider;

    @Spy
    private MockUserDataGenerator mockUserDataGenerator;

    @Test
    void shouldReturnUsernamePasswordAuthenticationTokenWithUserDetails() {

        // GIVEN
        JwtUser jwtUser = mockUserDataGenerator.jwtUserDto();
        String jwtToken = jwtToken(jwtUser.getUsername(), SCOPE);

        UsernamePasswordAuthenticationToken expectedAuthToken =
                new UsernamePasswordAuthenticationToken(jwtUser, "", jwtUser.getAuthorities());

        when(extractor.extractSubject(any())).thenReturn(jwtUser.getUsername());
        when(userDetailsService.loadUserByUsername(jwtUser.getUsername())).thenReturn(jwtUser);

        // WHEN
        Authentication authentication = authenticationTokenProvider.getAuthentication(jwtToken);

        // THEN
        assertThat(authentication).isExactlyInstanceOf(UsernamePasswordAuthenticationToken.class);
        assertEquals(expectedAuthToken, authentication);

    }

}
