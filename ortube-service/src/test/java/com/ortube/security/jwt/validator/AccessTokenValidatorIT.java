package com.ortube.security.jwt.validator;

import com.ortube.config.AbstractIntegrationTest;
import com.ortube.datagenerators.TokenDataGenerator;
import com.ortube.exceptions.JwtAuthenticationException;
import com.ortube.security.model.TokenScopes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.Map;

import static com.ortube.constants.HeadersKt.AUTHENTICATION_HEADER;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AccessTokenValidatorIT extends AbstractIntegrationTest {

    @Autowired
    private AccessTokenValidator accessTokenValidator;

    @Autowired
    private TokenDataGenerator tokenDataGenerator;

    @Value("${jwt.refreshToken.expired}")
    private long tokenTestExpirationTime;

    @Test
    void shouldReturnTrueIfTokenIsValid() {

        // GIVEN
        String jwtToken = tokenDataGenerator.jwtToken("Subject", TokenScopes.ACCESS_TOKEN.getValue());

        Map<String, String> headers = Map.of(AUTHENTICATION_HEADER, jwtToken);

        // WHEN
        boolean result = accessTokenValidator.validateToken(headers);

        // THEN
        assertTrue(result);

    }

    @Test
    void shouldThrowJwtAuthenticationExceptionIfTokenIsExpired() throws InterruptedException {

        // GIVEN
        String jwtToken = tokenDataGenerator.jwtToken("Subject", "access");
        Map<String, String> headers = Map.of(AUTHENTICATION_HEADER, jwtToken);

        // WHEN
        Thread.sleep(tokenTestExpirationTime * 1000);

        // THEN
        assertThrows(JwtAuthenticationException.class, () -> accessTokenValidator.validateToken(headers));

    }

    @Test
    void shouldThrowJwtAuthenticationExceptionIfTokenIsInvalid() {

        // GIVEN
        Map<String, String> headers = Map.of(AUTHENTICATION_HEADER, "invalid token");

        // WHEN THEN
        assertThrows(JwtAuthenticationException.class, () -> accessTokenValidator.validateToken(headers));

    }
}
