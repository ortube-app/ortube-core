package com.ortube.security.jwt.validator;

import com.ortube.config.AbstractIntegrationTest;
import com.ortube.security.jwt.factory.TokenValidatorFactory;
import com.ortube.security.model.TokenScopes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TokenValidatorFactoryIT extends AbstractIntegrationTest {

    @Autowired
    private TokenValidatorFactory tokenValidatorFactory;

    @Test
    void shouldReturnAccessTokenValidator() {

        // GIVEN
        TokenScopes accessTokenScope = TokenScopes.ACCESS_TOKEN;

        // WHEN
        TokenValidator tokenValidator = tokenValidatorFactory.getTokenValidator(accessTokenScope);

        // THEN
        assertThat(tokenValidator).isExactlyInstanceOf(AccessTokenValidator.class);
    }

    @Test
    void shouldReturnRefreshTokenValidator() {

        // GIVEN
        TokenScopes accessTokenScope = TokenScopes.REFRESH_TOKEN;

        // WHEN
        TokenValidator tokenValidator = tokenValidatorFactory.getTokenValidator(accessTokenScope);

        // THEN
        assertThat(tokenValidator).isExactlyInstanceOf(RefreshTokenValidator.class);
    }
}
