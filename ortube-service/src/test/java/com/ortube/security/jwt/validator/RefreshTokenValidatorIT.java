package com.ortube.security.jwt.validator;

import com.ortube.config.AbstractIntegrationTest;
import com.ortube.datagenerators.MockUserDataGenerator;
import com.ortube.datagenerators.TokenDataGenerator;
import com.ortube.persistence.domain.RefreshToken;
import com.ortube.persistence.domain.User;
import com.ortube.security.model.TokenScopes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

import static com.ortube.constants.HeadersKt.AUTHENTICATION_HEADER;
import static com.ortube.constants.HeadersKt.BROWSER_FINGERPRINT_HEADER;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RefreshTokenValidatorIT extends AbstractIntegrationTest {

    @Autowired
    private RefreshTokenValidator refreshTokenValidator;

    @Autowired
    private TokenDataGenerator tokenDataGenerator;

    @Autowired
    private MockUserDataGenerator mockUserDataGenerator;

    @Test
    void shouldReturnTrueIfTokenIsValid() {

        // GIVEN
        User user = mockUserDataGenerator.userWithoutId();

        String jwtToken = tokenDataGenerator.jwtToken(user.getUsername(), TokenScopes.REFRESH_TOKEN.getValue());
        String fingerprint = randomAlphabetic(10);

        user.setRefreshTokens(List.of(new RefreshToken(jwtToken, fingerprint)));

        mongoTemplate.save(user);

        Map<String, String> headers = Map.of(
                AUTHENTICATION_HEADER, jwtToken,
                BROWSER_FINGERPRINT_HEADER, fingerprint
        );

        // WHEN
        boolean result = refreshTokenValidator.validateToken(headers);

        // THEN
        assertTrue(result);

    }

    @Test
    void shouldReturnFalseIfUserDoNotOwnsToken() {

        // GIVEN
        User user = mockUserDataGenerator.userWithoutId();

        String jwtToken = tokenDataGenerator.jwtToken(user.getUsername(), TokenScopes.REFRESH_TOKEN.getValue());
        String fingerprint = randomAlphabetic(10);

        user.setRefreshTokens(List.of(new RefreshToken(jwtToken, fingerprint)));

        mongoTemplate.save(user);

        Map<String, String> headers = Map.of(
                AUTHENTICATION_HEADER, jwtToken,
                BROWSER_FINGERPRINT_HEADER, "wrong fingerprint"
        );

        // WHEN
        boolean result = refreshTokenValidator.validateToken(headers);

        // THEN
        assertFalse(result);

    }

}
