package com.ortube.security.jwt.provider.token;

import com.ortube.config.AbstractUnitTest;
import com.ortube.exceptions.MissingRequiredArgumentException;
import com.ortube.security.jwt.JwtUtils;
import com.ortube.security.model.TokenScopes;
import io.jsonwebtoken.Claims;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.internal.util.reflection.FieldSetter;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class JwtTokenProviderTest extends AbstractUnitTest {

    private static final long ACCESS_TOKEN_EXPIRATION_TIME = 2;
    private static final long REFRESH_TOKEN_EXPIRATION_TIME = 3;

    private static JwtTokenProviderImpl jwtTokenProvider = new JwtTokenProviderImpl();

    @BeforeAll
    static void mockFields() throws NoSuchFieldException {
        FieldSetter.setField(jwtTokenProvider,
                jwtTokenProvider.getClass().getDeclaredField("tokenSigningKey"),
                MOCK_TOKEN_SIGNING_KEY);

        FieldSetter.setField(jwtTokenProvider,
                jwtTokenProvider.getClass().getDeclaredField("accessTokenExpirationTime"),
                ACCESS_TOKEN_EXPIRATION_TIME);

        FieldSetter.setField(jwtTokenProvider,
                jwtTokenProvider.getClass().getDeclaredField("refreshTokenExpirationTime"),
                REFRESH_TOKEN_EXPIRATION_TIME);
    }

    @Test
    void shouldCreateAccessToken() {
        // GIVEN
        String subject = RandomStringUtils.randomAlphabetic(20);

        // WHEN
        String token = jwtTokenProvider.createAccessToken(subject);

        String actualSubject = getTokenBody(token).getSubject();
        Date actualExpiration = getTokenBody(token).getExpiration();
        TokenScopes actualScope = TokenScopes.fromString((String) getTokenBody(token).get("scope"));

        // THEN
        assertEquals(subject, actualSubject);
        assertEquals(TokenScopes.ACCESS_TOKEN, actualScope);
        assertThat(actualExpiration).isBefore(Timestamp.valueOf(LocalDateTime.now().plusSeconds(ACCESS_TOKEN_EXPIRATION_TIME)));
    }

    @Test
    void shouldThrowMissingRequiredArgumentExceptionWhenSubjectNotPresent() {
        assertThrows(MissingRequiredArgumentException.class, () -> jwtTokenProvider.createAccessToken(null));
    }

    @Test
    void shouldCreateRefreshToken() {
        // GIVEN
        String subject = RandomStringUtils.randomAlphabetic(20);

        // WHEN
        String token = jwtTokenProvider.createRefreshToken(subject);

        String actualSubject = getTokenBody(token).getSubject();
        Date actualExpiration = getTokenBody(token).getExpiration();
        TokenScopes actualScope = TokenScopes.fromString((String) getTokenBody(token).get("scope"));

        // THEN
        assertEquals(subject, actualSubject);
        assertEquals(TokenScopes.REFRESH_TOKEN, actualScope);
        assertThat(actualExpiration).isBefore(Timestamp.valueOf(LocalDateTime.now().plusSeconds(REFRESH_TOKEN_EXPIRATION_TIME)));
    }

    private Claims getTokenBody(String token) {
        return JwtUtils.parse(MOCK_TOKEN_SIGNING_KEY, token);
    }

}
