package com.ortube.validation.validator;

import com.ortube.config.AbstractUnitTest;
import com.ortube.datagenerators.MockUserDataGenerator;
import com.ortube.persistence.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;

class UniqueEmailValidatorTest extends AbstractUnitTest {

    private final MockUserDataGenerator mockUserDataGenerator = new MockUserDataGenerator();

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UniqueEmailValidator uniqueEmailValidator;

    @Test
    void shouldReturnTrueIfUserByEmailNotFound() {
        // GIVEN
        var email = "email@gmail.com";
        given(userRepository.findByEmail(email)).willReturn(Optional.empty());

        // WHEN
        boolean valid = uniqueEmailValidator.isValid(email, null);

        // THEN
        assertTrue(valid);
    }

    @Test
    void shouldReturnFalseIfUserByEmailFound() {
        // GIVEN
        var user = mockUserDataGenerator.user();
        var email = user.getEmail();
        given(userRepository.findByEmail(email)).willReturn(Optional.of(user));

        // WHEN
        boolean valid = uniqueEmailValidator.isValid(email, null);

        // THEN
        assertFalse(valid);
    }

    @Test
    void shouldReturnFalseIfEmailIsNull() {
        // GIVEN
        String userEmail = null;

        // WHEN
        boolean valid = uniqueEmailValidator.isValid(userEmail, null);

        // THEN
        assertFalse(valid);
    }

    @Test
    void shouldReturnFalseIfEmailIsEmpty() {
        // GIVEN
        String userEmail = "";

        // WHEN
        boolean valid = uniqueEmailValidator.isValid(userEmail, null);

        // THEN
        assertFalse(valid);
    }

}