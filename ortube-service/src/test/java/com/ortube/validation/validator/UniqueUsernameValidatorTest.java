package com.ortube.validation.validator;

import com.ortube.config.AbstractUnitTest;
import com.ortube.datagenerators.MockUserDataGenerator;
import com.ortube.persistence.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;

class UniqueUsernameValidatorTest extends AbstractUnitTest {

    private final MockUserDataGenerator mockUserDataGenerator = new MockUserDataGenerator();

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UniqueUsernameValidator uniqueUsernameValidator;

    @Test
    void shouldReturnTrueIfUserByEmailNotFound() {
        // GIVEN
        var username = "username";
        given(userRepository.findByUsername(username)).willReturn(Optional.empty());

        // WHEN
        boolean valid = uniqueUsernameValidator.isValid(username, null);

        // THEN
        assertTrue(valid);
    }

    @Test
    void shouldReturnFalseIfUserByEmailFound() {
        // GIVEN
        var user = mockUserDataGenerator.user();
        var username = user.getUsername();
        given(userRepository.findByUsername(username)).willReturn(Optional.of(user));

        // WHEN
        boolean valid = uniqueUsernameValidator.isValid(username, null);

        // THEN
        assertFalse(valid);
    }

    @Test
    void shouldReturnFalseIfUsernameIsNull() {
        // GIVEN
        String username = null;

        // WHEN
        boolean valid = uniqueUsernameValidator.isValid(username, null);

        // THEN
        assertFalse(valid);
    }

    @Test
    void shouldReturnFalseIfUsernameIsEmpty() {
        // GIVEN
        String username = "";

        // WHEN
        boolean valid = uniqueUsernameValidator.isValid(username, null);

        // THEN
        assertFalse(valid);
    }

}