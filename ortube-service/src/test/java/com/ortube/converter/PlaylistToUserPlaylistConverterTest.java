package com.ortube.converter;

import com.ortube.config.AbstractUnitTest;
import com.ortube.converters.PlaylistToUserPlaylistConverter;
import com.ortube.datagenerators.MockPlaylistDataGenerator;
import com.ortube.persistence.domain.Playlist;
import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.persistence.domain.enums.PlaylistPrivacy;
import com.ortube.security.model.UserContextHolder;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

public class PlaylistToUserPlaylistConverterTest extends AbstractUnitTest {

    private final MockPlaylistDataGenerator mockPlaylistDataGenerator = new MockPlaylistDataGenerator();

    @Mock
    private UserContextHolder userContextHolder;

    @InjectMocks
    private PlaylistToUserPlaylistConverter playlistToUserPlaylistConverter;

    @Test
    void shouldConvertPlaylistToUserPlaylist() {
        // GIVEN
        given(userContextHolder.getUserId()).willReturn(ObjectId.get().toHexString());
        Playlist playlist = mockPlaylistDataGenerator.playlist();

        // WHEN
        UserPlaylist userPlaylist = playlistToUserPlaylistConverter.convert(playlist);

        // THEN
        assertEquals(playlist.getTitle(), userPlaylist.getTitle());
        assertEquals(playlist.getTopic(), userPlaylist.getTopic());
        assertEquals(PlaylistPrivacy.PRIVATE, userPlaylist.getPrivacy());
        assertEquals(playlist.getWords(), userPlaylist.getWords());
    }
}
