package com.ortube.converter

import com.ortube.config.AbstractUnitTest
import com.ortube.converters.RequestDtoToWordConverter
import com.ortube.datagenerators.WordDataGenerator
import com.ortube.dto.WordRequestDto
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.Spy

class RequestDtoToWordConverterTest : AbstractUnitTest() {

    @Spy
    lateinit var requestDtoToWordConverter: RequestDtoToWordConverter

    @Spy
    lateinit var wordDataGenerator: WordDataGenerator

    @Test
    fun shouldConvertRequestDtoToWord() {
        // GIVEN
        val expectedWord = wordDataGenerator.word()
        val wordRequestDto = WordRequestDto(expectedWord.lang1, expectedWord.lang2, expectedWord.image)

        // WHEN
        val actualWord = requestDtoToWordConverter.convert(wordRequestDto)

        // THEN
        assertEquals(expectedWord.lang1, actualWord.lang1)
        assertEquals(expectedWord.lang2, actualWord.lang2)
        assertEquals(expectedWord.image, actualWord.image)
    }

}