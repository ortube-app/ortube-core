package com.ortube.converter;

import com.ortube.config.AbstractUnitTest;
import com.ortube.converters.RequestDtoToUserPlaylistConverter;
import com.ortube.datagenerators.MockUserPlaylistDataGenerator;
import com.ortube.dto.UserPlaylistRequestDto;
import com.ortube.persistence.domain.UserPlaylist;
import com.ortube.security.model.UserContextHolder;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

public class RequestDtoToUserPlaylistConverterTest extends AbstractUnitTest {

    private final MockUserPlaylistDataGenerator mockUserPlaylistDataGenerator = new MockUserPlaylistDataGenerator();

    @Mock
    private UserContextHolder userContextHolder;

    @InjectMocks
    private RequestDtoToUserPlaylistConverter requestDtoToUserPlaylistConverter;

    @Test
    void shouldConvertRequestDtoToUserPlaylist() {
        // GIVEN
        given(userContextHolder.getUserId()).willReturn(ObjectId.get().toHexString());
        UserPlaylistRequestDto userPlaylistRequestDto = mockUserPlaylistDataGenerator.userPlaylistRequestDto();

        // WHEN
        UserPlaylist userPlaylist = requestDtoToUserPlaylistConverter.convert(userPlaylistRequestDto);

        // THEN
        assertEquals(userPlaylistRequestDto.getTitle(), userPlaylist.getTitle());
        assertEquals(userPlaylistRequestDto.getTopic(), userPlaylist.getTopic());
        assertEquals(userPlaylistRequestDto.getPrivacy(), userPlaylist.getPrivacy());
    }
}
