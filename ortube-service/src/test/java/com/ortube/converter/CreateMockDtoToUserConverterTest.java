package com.ortube.converter;

import com.ortube.config.AbstractUnitTest;
import com.ortube.converters.CreateMockDtoToUserConverter;
import com.ortube.datagenerators.MockUserDataGenerator;
import com.ortube.dto.MockUserCreateDto;
import com.ortube.persistence.domain.User;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class CreateMockDtoToUserConverterTest extends AbstractUnitTest {

    @Spy
    private CreateMockDtoToUserConverter createMockDtoToUserConverter;

    @Spy
    private MockUserDataGenerator mockUserDataGenerator;

    @Test
    void shouldConvertCreateDtoToUser() {

        // GIVEN
        MockUserCreateDto mockUserCreateDto = mockUserDataGenerator.mockUserCreateDto();

        // WHEN
        User user = createMockDtoToUserConverter.convert(mockUserCreateDto);

        // THEN
        assertNull(user.getId());
        assertEquals(mockUserCreateDto.getEmail(), user.getEmail());
        assertEquals(mockUserCreateDto.getUserName(), user.getUsername());
        assertEquals(mockUserCreateDto.getNickName(), user.getNickname());
        assertEquals(mockUserCreateDto.getRole(), user.getUserRole());
        assertEquals(mockUserCreateDto.getPassword(), user.getPassword());

    }
}
