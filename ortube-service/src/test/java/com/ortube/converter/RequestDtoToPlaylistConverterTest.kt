package com.ortube.converter

import com.ortube.config.AbstractUnitTest
import com.ortube.converters.RequestDtoToPlaylistConverter
import com.ortube.dto.PlaylistRequestDto
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.mockito.Spy

class RequestDtoToPlaylistConverterTest : AbstractUnitTest() {

    @Spy
    private lateinit var requestDtoToPlaylistConverter: RequestDtoToPlaylistConverter

    @Test
    fun `Should convert CreateDto to Playlist`() {

        // GIVEN
        val playlistRequestDto = PlaylistRequestDto(title = "title", topic = "topic")

        // WHEN
        val playlist = requestDtoToPlaylistConverter.convert(playlistRequestDto)

        // THEN
        assertEquals(playlistRequestDto.title, playlist.title)
        assertEquals(playlistRequestDto.topic, playlist.topic)

    }

    @Test
    fun `Should return null topic if topic not present`() {

        // GIVEN
        val playlistRequestDto = PlaylistRequestDto(title = "title")

        // WHEN
        val playlist = requestDtoToPlaylistConverter.convert(playlistRequestDto)

        // THEN
        assertEquals(playlistRequestDto.title, playlist.title)
        assertNull(playlist.topic)

    }
}