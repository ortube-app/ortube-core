package com.ortube.converter

import com.ortube.config.AbstractUnitTest
import com.ortube.converters.UserPlaylistToBriefDtoConverter
import com.ortube.converters.WordToBriefDtoConverter
import com.ortube.datagenerators.MockUserPlaylistDataGenerator
import com.ortube.dto.WordBriefDto
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class UserPlaylistToBriefDtoConverterTest : AbstractUnitTest() {

    private val mockUserPlaylistDataGenerator = MockUserPlaylistDataGenerator()

    @Mock
    lateinit var wordToBriefDtoConverter: WordToBriefDtoConverter

    @InjectMocks
    lateinit var userPlaylistToBriefDtoConverter: UserPlaylistToBriefDtoConverter

    @Test
    fun `Should convert UserPlaylist to brief dto`() {
        // GIVEN
        given(wordToBriefDtoConverter.convert(any())).willReturn(mock(WordBriefDto::class.java))
        val userPlaylist = mockUserPlaylistDataGenerator.userPlaylist()

        // WHEN
        val (id, title, topic, image, privacy, wordCount) = userPlaylistToBriefDtoConverter.convert(userPlaylist)

        // THEN
        assertThat(id).isEqualTo(userPlaylist.id)
        assertThat(title).isEqualTo(userPlaylist.title)
        assertThat(topic).isEqualTo(userPlaylist.topic)
        assertThat(image).isEqualTo(userPlaylist.image)
        assertThat(privacy).isEqualTo(userPlaylist.privacy)
        assertThat(wordCount).isEqualTo(userPlaylist.words.size)
        verify(wordToBriefDtoConverter, times(3)).convert(any())
    }

    @Test
    fun `Should convert UserPlaylist to brief dto with null image`() {
        // GIVEN
        given(wordToBriefDtoConverter.convert(any())).willReturn(mock(WordBriefDto::class.java))
        val userPlaylist = mockUserPlaylistDataGenerator.userPlaylist(image = null)

        // WHEN
        val (_, _, _, image) = userPlaylistToBriefDtoConverter.convert(userPlaylist)

        // THEN
        assertThat(image).isNull()
    }

    @Test
    fun `Should convert UserPlaylist to brief dto with null topic`() {
        // GIVEN
        given(wordToBriefDtoConverter.convert(any())).willReturn(mock(WordBriefDto::class.java))
        val userPlaylist = mockUserPlaylistDataGenerator.userPlaylist(topic = null)

        // WHEN
        val (_, _, topic) = userPlaylistToBriefDtoConverter.convert(userPlaylist)

        // THEN
        assertThat(topic).isNull()
    }

}