package com.ortube.converter;

import com.ortube.config.AbstractUnitTest;
import com.ortube.converters.JwtUserToUserAccountDtoConverter;
import com.ortube.datagenerators.MockUserDataGenerator;
import com.ortube.dto.UserAccountDto;
import com.ortube.security.model.JwtUser;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JwtUserToUserAccountDtoConverterTest extends AbstractUnitTest {

    @Spy
    private JwtUserToUserAccountDtoConverter jwtUserToUserAccountDtoConverter;

    @Spy
    private MockUserDataGenerator mockUserDataGenerator;

    @Test
    void shouldConvertJwtUserToUserAccountDto() {

        // GIVEN
        JwtUser jwtUser = mockUserDataGenerator.jwtUserDto();

        // WHEN
        UserAccountDto userAccountDto = jwtUserToUserAccountDtoConverter.convert(jwtUser);

        // THEN
        assertEquals(jwtUser.getId(), userAccountDto.getId());
        assertEquals(jwtUser.getEmail(), userAccountDto.getEmail());
        assertEquals(jwtUser.getUsername(), userAccountDto.getUsername());
        assertEquals(jwtUser.getNickname(), userAccountDto.getNickname());
        assertEquals(jwtUser.getAuthorities().stream().findFirst().get().getAuthority(), userAccountDto.getRole().name());

    }
}
