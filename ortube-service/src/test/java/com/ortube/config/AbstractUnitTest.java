package com.ortube.config;

import com.ortube.security.jwt.JwtUtils;
import com.ortube.security.model.TokenScopes;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

@ExtendWith(MockitoExtension.class)
public abstract class AbstractUnitTest {

    protected static final String MOCK_TOKEN_SIGNING_KEY = "J@NcRfUjXn2r5u8x/A%D*G-KaPdSgVkYp3s6v9y$B&E(H+MbQeThWmZq4t7w!z%";
    protected static final String USER_USERNAME = "User";
    protected static final String SCOPE = TokenScopes.ACCESS_TOKEN.getValue();
    protected static final long TOKEN_EXPIRATION_TIME = 5;

    protected String jwtToken(String subject, String scope) {
        Claims claims = Jwts.claims().setSubject(subject);
        claims.put("scope", scope);
        LocalDateTime currentTime = LocalDateTime.now();
        return JwtUtils.build(claims, currentTime, TOKEN_EXPIRATION_TIME, MOCK_TOKEN_SIGNING_KEY);
    }

}
