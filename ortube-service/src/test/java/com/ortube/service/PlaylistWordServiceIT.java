package com.ortube.service;

import com.ortube.config.AbstractIntegrationTest;
import com.ortube.datagenerators.PlaylistDataGenerator;
import com.ortube.datagenerators.WordDataGenerator;
import com.ortube.dto.WordRequestDto;
import com.ortube.exceptions.WordNotFoundException;
import com.ortube.persistence.domain.Playlist;
import com.ortube.persistence.domain.Word;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PlaylistWordServiceIT extends AbstractIntegrationTest {

    @Autowired
    private PlaylistWordService wordService;

    @Autowired
    private PlaylistDataGenerator playlistDataGenerator;

    @Autowired
    private WordDataGenerator wordDataGenerator;

    @Test
    void shouldThrowWordNotFoundOnUpdateWordNotExistingWord() {
        // GIVEN
        Playlist playlist = playlistDataGenerator.playlist();

        // WHEN THEN
        assertThrows(WordNotFoundException.class, () ->
                wordService.update(playlist.getId(), "-1", null));
    }

    @Test
    void shouldThrowWordNotFoundOnDeleteNotExistingWord() {
        // GIVEN
        Playlist playlist = playlistDataGenerator.playlist();

        // WHEN THEN
        assertThrows(WordNotFoundException.class, () -> wordService.deleteByIdAndPlaylistId("-1", playlist.getId()));
    }

    @Test
    void shouldAddWordToPlaylist() {
        // GIVEN
        Playlist playlist = playlistDataGenerator.playlist();
        WordRequestDto wordToAdd = wordDataGenerator.wordRequestDto();

        // WHEN
        Playlist actualPlaylist = wordService.addWordToPlaylist(playlist.getId(), wordToAdd);

        List<Word> wordsMatchesWordToAdd = actualPlaylist.getWords().stream()
                .filter(word -> word.getLang1().equals(wordToAdd.getLang1())
                        && word.getLang2().equals(wordToAdd.getLang2()))
                .collect(toList());

        // THEN
        assertThat(actualPlaylist.getWords()).hasSize(playlist.getWords().size() + 1);
        assertThat(wordsMatchesWordToAdd).hasSize(1);

    }

    @Test
    void shouldUpdateWord() {
        // GIVEN
        Playlist playlist = playlistDataGenerator.playlist();
        Word originalWord = playlist.getWords().get(0);

        WordRequestDto newWord = new WordRequestDto("lang1", "lang2", "image");

        // WHEN
        wordService.update(playlist.getId(), originalWord.getId(), newWord);

        Word actualWord = mongoTemplate.findById(playlist.getId(), Playlist.class)
                .getWords().stream().filter(word -> word.getId().equals(originalWord.getId())).findFirst().get();

        // THEN
        assertEquals(newWord.getLang1(), actualWord.getLang1());
        assertEquals(newWord.getLang2(), actualWord.getLang2());
        assertEquals(newWord.getImage(), actualWord.getImage());
        assertNotEquals(originalWord.getLang1(), actualWord.getLang1());
        assertNotEquals(originalWord.getLang2(), actualWord.getLang2());
        assertEquals(originalWord.getId(), actualWord.getId());
    }

    @Test
    void shouldDeleteWord() {
        // GIVEN
        Playlist originalPlaylist = playlistDataGenerator.playlist();
        Word originalWord = originalPlaylist.getWords().get(0);

        // WHEN
        wordService.deleteByIdAndPlaylistId(originalWord.getId(), originalPlaylist.getId());
        Playlist actualPlaylist = mongoTemplate.findById(originalPlaylist.getId(), Playlist.class);

        // THEN
        assertThat(actualPlaylist.getWords()).hasSize(originalPlaylist.getWords().size() - 1);
        assertThat(actualPlaylist.getWords()).doesNotContain(originalWord);
    }

    @Test
    void shouldDeleteAllWordsByIdAndPlaylistId() {
        // GIVEN
        Playlist originalPlaylist = playlistDataGenerator.playlist();
        Word originalWord1 = originalPlaylist.getWords().get(0);
        Word originalWord2 = originalPlaylist.getWords().get(1);

        // WHEN
        wordService.deleteAllByIdAndPlaylistId(
                List.of(originalWord1.getId(), originalWord2.getId()),
                originalPlaylist.getId()
        );

        Playlist actualPlaylist = mongoTemplate.findById(originalPlaylist.getId(), Playlist.class);

        // THEN
        assertThat(actualPlaylist.getWords()).hasSize(originalPlaylist.getWords().size() - 2);
        assertThat(actualPlaylist.getWords()).doesNotContain(originalWord1, originalWord2);
    }

}
