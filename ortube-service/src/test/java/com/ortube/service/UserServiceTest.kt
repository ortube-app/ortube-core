package com.ortube.service

import com.ortube.config.AbstractUnitTest
import com.ortube.datagenerators.MockUserDataGenerator
import com.ortube.exceptions.UserNotFoundException
import com.ortube.persistence.domain.projection.CommonUserProjection
import com.ortube.persistence.repository.UserRepository
import com.ortube.service.impl.UserServiceImpl
import com.ortube.toOptional
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.any
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.security.core.userdetails.UsernameNotFoundException
import java.util.Optional

class UserServiceTest : AbstractUnitTest() {

    private val mockUserDataGenerator = MockUserDataGenerator()

    @Mock
    lateinit var userRepository: UserRepository

    @InjectMocks
    lateinit var userService: UserServiceImpl

    @Test
    fun `Should invoke user repository when saving user`() {

        // GIVEN
        val user = mockUserDataGenerator.user()
        given(userRepository.save(user)).willReturn(user)

        // WHEN
        val savedUser = userService.save(user)

        // THEN
        assertEquals(user, savedUser)
        verify(userRepository, times(1)).save(any())
    }

    @Test
    fun `Should throw UsernameNotFoundException if user not found`() {
        // GIVEN
        val wrongUsername = "Wrong username"
        given(userRepository.findByEmailOrUsername(wrongUsername, wrongUsername))
                .willThrow(UsernameNotFoundException::class.java)

        // WHEN THEN
        assertThrows(UsernameNotFoundException::class.java) { userService.findByEmailOrUsername(wrongUsername) }
        verify(userRepository, times(1)).findByEmailOrUsername(wrongUsername, wrongUsername)
    }

    @Test
    fun `Should return user`() {
        // GIVEN
        val user = mockUserDataGenerator.user()
        given(userRepository.findByEmailOrUsername(user.username, user.username)).willReturn(user.toOptional())

        // WHEN
        val foundUser = userService.findByEmailOrUsername(user.username)

        // THEN
        assertEquals(user, foundUser)
        verify(userRepository, times(1)).findByEmailOrUsername(user.username, user.username)
    }

    @Test
    fun `Should return user by id`() {
        // GIVEN
        val userProjection = mockUserDataGenerator.commonUserProjection()
        given(userRepository.findProjectionById(userProjection.id)).willReturn(userProjection.toOptional())

        // WHEN
        val foundUser = userService.findById(userProjection.id)

        // THEN
        assertEquals(userProjection, foundUser)
        verify(userRepository, times(1)).findProjectionById(userProjection.id)
    }

    @Test
    fun `Should throw UserNotFoundException if user not found`() {
        // GIVEN
        val id = "id"
        given(userRepository.findProjectionById(id)).willReturn(Optional.empty())

        // WHEN THEN
        assertThrows(UserNotFoundException::class.java) { userService.findById(id) }
    }

    @Test
    fun `Should find all`() {
        // GIVEN
        val page = 0
        val size = 10
        val userProjection = mockUserDataGenerator.commonUserProjection()
        val userProjections = listOf(userProjection)
        val pageImpl = PageImpl<CommonUserProjection>(userProjections, PageRequest.of(page, size), userProjections.size.toLong())
        given(userRepository.findAllCommonProjections(page, size)).willReturn(pageImpl)

        // WHEN
        val foundUsers = userService.findAll(page, size)

        // THEN
        assertThat(foundUsers.totalPages).isEqualTo(pageImpl.totalPages)
        assertThat(foundUsers.pageNumber).isEqualTo(pageImpl.number)
        assertThat(foundUsers.last).isEqualTo(pageImpl.isLast)
        assertThat(foundUsers.first).isEqualTo(pageImpl.isFirst)
        assertThat(foundUsers.pageSize).isEqualTo(size)
        assertThat(foundUsers.content).hasSameSizeAs(userProjections)
        verify(userRepository, times(1)).findAllCommonProjections(page, size)
    }

}