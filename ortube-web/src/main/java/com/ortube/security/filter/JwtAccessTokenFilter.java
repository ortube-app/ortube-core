package com.ortube.security.filter;

import com.ortube.security.jwt.extractor.TokenExtractor;
import com.ortube.security.jwt.factory.TokenValidatorFactory;
import com.ortube.security.jwt.provider.authentication.AuthenticationTokenProvider;
import com.ortube.security.model.TokenScopes;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.ortube.constants.HeadersKt.AUTHENTICATION_HEADER;
import static com.ortube.constants.HeadersKt.BROWSER_FINGERPRINT_HEADER;

@RequiredArgsConstructor
public class JwtAccessTokenFilter extends OncePerRequestFilter {

    private final TokenExtractor tokenExtractor;
    private final TokenValidatorFactory tokenValidatorFactory;
    private final AuthenticationTokenProvider authenticationTokenProvider;

    @Override
    public void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain filterChain)
            throws IOException, ServletException {

        String tokenPayload = req.getHeader(AUTHENTICATION_HEADER);
        String token = tokenExtractor.extractToken(tokenPayload);
        TokenScopes scope = tokenExtractor.extractScope(token);

        Map<String, String> headers = new HashMap<>();

        headers.put(AUTHENTICATION_HEADER, token);
        headers.put(BROWSER_FINGERPRINT_HEADER, req.getHeader(BROWSER_FINGERPRINT_HEADER));

        if (tokenValidatorFactory.getTokenValidator(scope).validateToken(headers)) {
            Authentication auth = authenticationTokenProvider.getAuthentication(token);
            SecurityContextHolder.getContext().setAuthentication(auth);
        }
        filterChain.doFilter(req, res);
    }

}
