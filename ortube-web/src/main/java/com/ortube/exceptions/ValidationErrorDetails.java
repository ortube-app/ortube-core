package com.ortube.exceptions;

import lombok.Getter;
import org.springframework.data.util.StreamUtils;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ortube.constants.ErrorMessagesKt.VALIDATION_FAILED_MSG;

@Getter
public class ValidationErrorDetails extends ErrorDetails {

    private final Map<String, List<String>> validationErrors;

    public ValidationErrorDetails(List<FieldError> fieldErrors) {
        super(HttpStatus.UNPROCESSABLE_ENTITY, VALIDATION_FAILED_MSG);
        this.validationErrors = fieldErrors
                .stream()
                .collect(StreamUtils.toMultiMap(FieldError::getField, FieldError::getDefaultMessage));
    }

    public ValidationErrorDetails(Set<ConstraintViolation<?>> constraintViolations) {
        this(
                constraintViolations
                        .stream()
                        .map(constraintViolation -> {
                            var pathIterator = constraintViolation.getPropertyPath().iterator();
                            return new FieldError(
                                    pathIterator.next().getName(),
                                    pathIterator.next().getName(),
                                    constraintViolation.getMessage()
                            );
                        })
                        .collect(Collectors.toList())
        );
    }
}
