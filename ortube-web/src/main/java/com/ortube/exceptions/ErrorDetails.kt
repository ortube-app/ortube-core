package com.ortube.exceptions

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.time.LocalDateTime

open class ErrorDetails(
    val timestamp: LocalDateTime,
    val status: Int,
    val message: String,
    val error: String
) {

    constructor(
        httpStatus: HttpStatus,
        message: String
    ) : this(
            LocalDateTime.now(),
            httpStatus.value(),
            message, httpStatus.reasonPhrase
    )

    @get:JsonIgnore
    val responseEntity: ResponseEntity<ErrorDetails>
        get() = ResponseEntity.status(status).body(this)
}