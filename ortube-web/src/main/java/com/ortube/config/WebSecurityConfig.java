package com.ortube.config;

import com.ortube.security.auth.JwtAuthEntryPoint;
import com.ortube.security.filter.CustomCorsFilter;
import com.ortube.security.filter.JwtAccessTokenFilter;
import com.ortube.security.jwt.extractor.TokenExtractor;
import com.ortube.security.jwt.factory.TokenValidatorFactory;
import com.ortube.security.jwt.provider.authentication.AuthenticationTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String AUTHENTICATION_URL = "/auth/login";

    private final TokenExtractor tokenExtractor;
    private final TokenValidatorFactory tokenValidatorFactory;
    private final AuthenticationTokenProvider authenticationTokenProvider;
    private final JwtAuthEntryPoint jwtAuthEntryPoint;

    @Value("${security.skipEndpoints}")
    private String[] skipEndpoints;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(WebSecurity web) {
        web
                .ignoring()
                .antMatchers(AUTHENTICATION_URL)
                .antMatchers(skipEndpoints);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .csrf().disable()
                .exceptionHandling().authenticationEntryPoint(jwtAuthEntryPoint)

                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                .and()
                .authorizeRequests()
                .anyRequest().authenticated()

                .and()
                .addFilterBefore(new CustomCorsFilter(), UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(buildJwtAccessTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    private JwtAccessTokenFilter buildJwtAccessTokenFilter() {
        return new JwtAccessTokenFilter(tokenExtractor, tokenValidatorFactory, authenticationTokenProvider);
    }

}
