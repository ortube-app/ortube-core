package com.ortube.config;

import com.ortube.constants.HeadersKt;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static java.util.Collections.singletonList;

@Configuration
@EnableSwagger2
public class Swagger2Config {

    @Bean
    public Docket api() {

        Parameter parameter = new ParameterBuilder()
                .name(HeadersKt.AUTHENTICATION_HEADER)
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .description("If sent must starts with '" + HeadersKt.HEADER_PREFIX + "'")
                .build();

        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors
                        .basePackage("com.ortube.controller"))
                .paths(PathSelectors.regex("/.*"))
                .build().apiInfo(apiEndPointsInfo())
                .globalOperationParameters(singletonList(parameter));
    }

    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("ORTUBE REST API")
                .description("Ortube REST API")
                .contact(new Contact("Oleksii Mutianov", "", "mutianov.o.o@gmail.com"))
                .version("1.0.0")
                .build();
    }

}
