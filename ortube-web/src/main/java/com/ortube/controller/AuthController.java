package com.ortube.controller;

import com.ortube.dto.AuthenticationResponseDto;
import com.ortube.dto.LoginRequestDto;
import com.ortube.dto.UserAccountDto;
import com.ortube.security.facade.AuthFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("auth")
@Api(description = "REST API for auth", tags = "Auth API")
public class AuthController {

    private final AuthFacade authFacade;

    @GetMapping("token")
    @ApiOperation("Update tokens")
    public AuthenticationResponseDto refreshToken() {
        return authFacade.refreshAccessToken();
    }

    @PostMapping("login")
    @ApiOperation("Login")
    @ApiImplicitParam(
            name = "Browser-Fingerprint",
            value = "Fingerprint of device logged in from. Must be sent from client.",
            paramType = "header",
            required = true
    )
    public AuthenticationResponseDto login(@Valid @RequestBody LoginRequestDto loginRequestDto) {
        return authFacade.authenticate(loginRequestDto);
    }

    @GetMapping("me")
    @ApiOperation("Get user info")
    public UserAccountDto getMe() {
        return authFacade.getMe();
    }

}
