package com.ortube.controller

import com.ortube.constants.enums.UploadFolderName
import com.ortube.service.ImageUploadService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile

@RestController
@Api(description = "REST API for image uploading", tags = ["Image upload API"])
class ImageUploadController(
    private val imageUploadService: ImageUploadService
) {

    @PostMapping("image/{folder}")
    @ApiOperation("Upload image")
    fun upload(@PathVariable folder: UploadFolderName, file: MultipartFile?) = imageUploadService.upload(folder, file)

}