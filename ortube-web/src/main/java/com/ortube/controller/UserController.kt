package com.ortube.controller

import com.ortube.service.UserService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.constraints.Max
import javax.validation.constraints.Min

@RestController
@RequestMapping("secure/users")
@Api(description = "REST API to work with users", tags = ["User API"])
@Validated
class UserController(
    private val userService: UserService
) {

    @GetMapping("{id}")
    @ApiOperation("Get user by id")
    fun getById(@PathVariable id: String) = userService.findById(id)

    @GetMapping
    @ApiOperation("Get all users pageable")
    fun getAll(
        @RequestParam(defaultValue = "0")
        @Min(0)
        page: Int,

        @RequestParam(defaultValue = "10")
        @Min(1) @Max(100)
        size: Int
    ) = userService.findAll(page, size)

}