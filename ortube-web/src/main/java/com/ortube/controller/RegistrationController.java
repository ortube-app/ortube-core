package com.ortube.controller;

import com.ortube.dto.MockUserCreateDto;
import com.ortube.dto.UserAccountDto;
import com.ortube.dto.UserCreateDto;
import com.ortube.service.RegistrationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Api(description = "REST API for registration", tags = "Registration API")
public class RegistrationController {

    private final RegistrationService registrationService;

    @PostMapping("register")
    @ApiOperation("Register user with email and password")
    public UserAccountDto register(@RequestBody @Valid UserCreateDto userCreateDto) {
        return registrationService.register(userCreateDto);
    }

    @PostMapping("activate/{activationCode}")
    public void activateAccount(@PathVariable String activationCode) {
        registrationService.activateAccount(activationCode);
    }

    @Deprecated
    @PostMapping("createMockUser")
    @ApiOperation("Create a user. Just for testing.")
    @ResponseStatus(HttpStatus.CREATED)
    public String createMockUser(@RequestBody @Valid MockUserCreateDto mockUserCreateDto) {
        return registrationService.createMockUser(mockUserCreateDto);
    }

}
