package com.ortube.controller;

import com.ortube.dto.WordRequestDto;
import com.ortube.persistence.domain.Playlist;
import com.ortube.security.annotation.Access;
import com.ortube.service.PlaylistWordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("secure/playlist")
@Api(tags = "Playlist API")
@Access.Admin
public class WordController {

    private final PlaylistWordService wordService;

    @PostMapping("{playlistId}/word")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Add word to playlist")
    public Playlist addWord(@PathVariable String playlistId,
                            @RequestBody @Valid WordRequestDto wordRequestDto) {
        return wordService.addWordToPlaylist(playlistId, wordRequestDto);
    }

    @PutMapping("{playlistId}/word/{wordId}")
    @ApiOperation("Update word in playlist")
    public Playlist updateWord(@PathVariable String playlistId,
                               @PathVariable String wordId,
                               @RequestBody @Valid WordRequestDto wordRequestDto) {

        return wordService.update(playlistId, wordId, wordRequestDto);
    }

    @DeleteMapping("{playlistId}/word/{wordId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation("Delete word from playlist by playlist id and word id")
    public void deleteWord(@PathVariable String playlistId,
                           @PathVariable String wordId) {

        wordService.deleteByIdAndPlaylistId(wordId, playlistId);
    }

    @DeleteMapping("{playlistId}/word")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation("Delete several words from playlist")
    public void deleteWords(@PathVariable String playlistId,
                            @ApiParam(value = "Array of word ids to delete", required = true)
                            @RequestBody List<String> wordIds) {

        wordService.deleteAllByIdAndPlaylistId(wordIds, playlistId);
    }

}
