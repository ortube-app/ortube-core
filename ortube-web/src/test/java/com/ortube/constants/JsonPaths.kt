package com.ortube.constants

const val ACCESS_TOKEN_PATH = "accessToken"
const val REFRESH_TOKEN_PATH = "refreshToken"
const val EXPIRED_AT_PATH = "expiredAt"
const val ROLE_PATH = "role"

const val MESSAGE_PATH = "message"
const val VALIDATION_ERRORS_PATH = "validationErrors"

const val WORDS_PATH = "words"
const val WORD_PATH = "word"

const val QUIZ_ID_PATH = "quizId"
const val ANSWER_STATUS_PATH = "answerStatus"
const val CORRECT_OPTIONS_PATH = "correctOptions"
const val USER_ANSWERS_PATH = "userAnswers"

const val IMAGE_LINK_PATH = "imageLink"
const val FOLDER_PATH = "folder"
const val SIZE_PATH = "size"

const val RESULTS_PATH = "result"
const val ANSWERS_PATH = "answers"
