package com.ortube.controller

import com.ortube.config.AbstractWebIntegrationTest
import com.ortube.constants.AUTHENTICATION_HEADER
import com.ortube.constants.HEADER_PREFIX
import com.ortube.constants.USERS_ENDPOINT
import com.ortube.constants.USER_NOT_FOUND_MSG
import com.ortube.constants.VALIDATION_FAILED_MSG
import com.ortube.dto.PageResult
import com.ortube.exceptions.ErrorDetails
import com.ortube.persistence.domain.projection.CommonUserProjection
import divRoundUp
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class UserControllerIT : AbstractWebIntegrationTest() {

    @Test
    fun `Should return user by id with playlistCount`() {
        // GIVEN
        val publicUserPlaylists = listOf(userPlaylistDataGenerator.userPlaylist(user.id!!))

        // WHEN
        val request = get("$USERS_ENDPOINT/${user.id}")
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")
                .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
                .andExpect(jsonPath("$.${CommonUserProjection::id.name}").value(user.id!!))
                .andExpect(jsonPath("$.${CommonUserProjection::nickname.name}").value(user.nickname))
                .andExpect(jsonPath("$.${CommonUserProjection::username.name}").value(user.username))
                .andExpect(jsonPath("$.${CommonUserProjection::playlistCount.name}").value(publicUserPlaylists.size))
                .andExpect(jsonPath("$.${CommonUserProjection::image.name}").doesNotExist())
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return 404 if user by id not found`() {
        // GIVEN
        val invalidId = "invalid"

        // WHEN
        val request = get("$USERS_ENDPOINT/$invalidId")
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")
                .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
                .andExpect(jsonPath("$.${ErrorDetails::message.name}").value(USER_NOT_FOUND_MSG))
                .andExpect(status().isNotFound)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `Should return pageable user list`() {
        // GIVEN
        val users = generateSequence { userDataGenerator.user() }.take(23).toList()
        val page = 0
        val size = 9

        // WHEN
        val request = get(USERS_ENDPOINT)
                .param("page", page.toString())
                .param("size", size.toString())
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")
                .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
                .andExpect(jsonPath("$.${PageResult<*>::content.name}", hasSize<Any>(size)))
                .andExpect(jsonPath("$.${PageResult<*>::first.name}").value("true"))
                .andExpect(jsonPath("$.${PageResult<*>::last.name}").value("false"))
                .andExpect(jsonPath("$.${PageResult<*>::pageSize.name}").value(size))
                .andExpect(jsonPath("$.${PageResult<*>::pageNumber.name}").value(page))
                .andExpect(jsonPath("$.${PageResult<*>::totalPages.name}").value(users.size.divRoundUp(size)))
    }

    @Test
    fun `Should return 422 if page and size are invalid`() {
        // GIVEN
        val page = -1
        val size = -1

        // WHEN
        val request = get(USERS_ENDPOINT)
                .param("page", page.toString())
                .param("size", size.toString())
                .header(AUTHENTICATION_HEADER, "$HEADER_PREFIX$userAccessToken")
                .contentType(MediaType.APPLICATION_JSON)

        // THEN
        mockMvc.perform(request)
                .andExpect(jsonPath("$.${ErrorDetails::message.name}").value(VALIDATION_FAILED_MSG))
                .andExpect(status().isUnprocessableEntity)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
    }

}