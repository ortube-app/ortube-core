package com.ortube.exceptions

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito.RETURNS_DEEP_STUBS
import org.mockito.Mockito.mock
import org.springframework.validation.FieldError
import javax.validation.ConstraintViolation

class ValidationErrorDetailsTest {

    companion object {
        private const val OBJECT_NAME = "someObject"
    }

    @Test
    fun `Should contain correct field name and error when there is only one field with one error`() {

        // GIVEN
        val fieldName = "someField"
        val message = "must be not null"
        val fieldError = FieldError(OBJECT_NAME, fieldName, message)

        val validationErrorDetails = ValidationErrorDetails(listOf(fieldError))

        // WHEN
        val validationErrors = validationErrorDetails.validationErrors

        // THEN
        assertThat(validationErrors).containsKey(fieldName)
        assertThat(validationErrors[fieldName]).containsOnly(message)
    }

    @Test
    fun `Should contain correct field names and errors when there are two fields with one error for each`() {

        // GIVEN
        val fieldName1 = "someField1"
        val message1 = "must be not null"
        val fieldError1 = FieldError(OBJECT_NAME, fieldName1, message1)

        val fieldName2 = "someField2"
        val message2 = "cannot be empty"
        val fieldError2 = FieldError(OBJECT_NAME, fieldName2, message2)

        val validationErrorDetails = ValidationErrorDetails(listOf(fieldError1, fieldError2))

        // WHEN
        val validationErrors = validationErrorDetails.validationErrors

        // THEN
        assertThat(validationErrors).containsKeys(fieldName1, fieldName2)
        assertThat(validationErrors[fieldName1]).containsOnly(message1)
        assertThat(validationErrors[fieldName2]).containsOnly(message2)
    }

    @Test
    fun `Should contain correct field name and errors when there is only one field with multiple errors`() {

        // GIVEN
        val fieldName = "someField"
        val message1 = "must be not null"
        val message2 = "cannot be empty"

        val fieldError1 = FieldError(OBJECT_NAME, fieldName, message1)
        val fieldError2 = FieldError(OBJECT_NAME, fieldName, message2)

        val validationErrorDetails = ValidationErrorDetails(listOf(fieldError1, fieldError2))

        // WHEN
        val validationErrors = validationErrorDetails.validationErrors

        // THEN
        assertThat(validationErrors).containsKey(fieldName)
        assertThat(validationErrors[fieldName]).containsOnly(message1, message2)
    }

    @Test
    fun `Should work correctly with ConstraintViolation class`() {
        // GIVEN
        val fieldName = "someField"
        val message = "cannot be empty"
        val constraintViolation = mock(ConstraintViolation::class.java, RETURNS_DEEP_STUBS)

        given(constraintViolation.propertyPath.iterator().next().name).willReturn(fieldName)
        given(constraintViolation.message).willReturn(message)

        val validationErrorDetails = ValidationErrorDetails(setOf(constraintViolation))

        // WHEN
        val validationErrors = validationErrorDetails.validationErrors

        // THEN
        assertThat(validationErrors).containsKey(fieldName)
        assertThat(validationErrors[fieldName]).containsOnly(message)
    }

}