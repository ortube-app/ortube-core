package com.ortube.datagenerators

import com.ortube.dto.UserPlaylistRequestDto
import com.ortube.persistence.domain.UserPlaylist
import com.ortube.persistence.domain.enums.PlaylistPrivacy
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.bson.types.ObjectId
import org.springframework.context.annotation.Profile
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Component

@Component
@Profile("integration-test")
class UserPlaylistDataGenerator(
    private val mongoTemplate: MongoTemplate,
    private val wordDataGenerator: WordDataGenerator
) {

    fun userPlaylist(
        userId: String,
        privacy: PlaylistPrivacy = PlaylistPrivacy.PUBLIC,
        wordCount: Int = 5
    ): UserPlaylist {
        val userPlaylist = UserPlaylist(
                title = randomAlphabetic(30),
                topic = randomAlphabetic(30),
                privacy = privacy,
                userId = userId,
                words = wordDataGenerator.words(wordCount).apply { forEach { it.id = ObjectId.get().toHexString() } }
        )

        return mongoTemplate.save(userPlaylist)
    }

    fun userPlaylistRequestDto() = UserPlaylistRequestDto.builder()
            .topic(randomAlphabetic(30))
            .title(randomAlphabetic(30))
            .privacy(PlaylistPrivacy.PUBLIC)
            .build()!!

}