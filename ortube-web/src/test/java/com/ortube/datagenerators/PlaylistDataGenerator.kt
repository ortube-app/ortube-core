package com.ortube.datagenerators

import com.ortube.dto.PlaylistRequestDto
import com.ortube.persistence.domain.Playlist
import org.apache.commons.lang3.RandomStringUtils
import org.bson.types.ObjectId
import org.springframework.context.annotation.Profile
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Component
import randomPlaylistsImageUrl

@Component
@Profile("integration-test")
class PlaylistDataGenerator(
    private val wordDataGenerator: WordDataGenerator,
    private val mongoTemplate: MongoTemplate
) {

    fun playlist() = mongoTemplate.save(mockPlaylist().apply {
        words.forEach { it.id = ObjectId.get().toHexString() }
    })

    fun playlistRequestDto(playlist: Playlist = mockPlaylist()) =
            PlaylistRequestDto(
                    title = playlist.title,
                    topic = playlist.topic,
                    image = playlist.image
            )

    fun mockPlaylist() = Playlist(
            title = RandomStringUtils.randomAlphabetic(30),
            topic = RandomStringUtils.randomAlphabetic(30),
            words = wordDataGenerator.words(),
            image = randomPlaylistsImageUrl()
    )

}