package com.ortube.datagenerators

import com.ortube.dto.UserCreateDto
import com.ortube.persistence.domain.User
import com.ortube.persistence.domain.enums.UserRole
import org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric
import org.bson.types.ObjectId
import org.springframework.context.annotation.Profile
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Component

@Component
@Profile("integration-test")
class UserDataGenerator(
    val mongoTemplate: MongoTemplate
) {

    fun user() = mongoTemplate.save(mockUser())

    fun mockUser() = User(
            id = ObjectId.get().toHexString(),
            email = "${randomAlphabetic(20)}@gmail.com",
            password = randomAlphabetic(20),
            nickname = randomAlphabetic(20),
            username = randomAlphabetic(20),
            userRole = UserRole.USER
    )

    fun userCreateDto() = UserCreateDto(
            username = randomAlphabetic(20),
            nickname = randomAlphabetic(20),
            email = "${randomAlphabetic(20)}@gmail.com",
            password = randomAlphanumeric(20)
    )
}