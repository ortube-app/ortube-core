stages:
  - build
  - check
  - docker-package
  - deploy-heroku
  - deploy-aws-ec2

image: bellsoft/liberica-openjdk-alpine:11.0.8

variables:
  MAVEN_OPTS: "-Dmaven.repo.local=./.m2/repository"
  CONTAINER_NAME: ortube
  DOCKER_ARGS: -d --rm -e TZ=Europe/Kiev --name $CONTAINER_NAME -p80:8080
  CI_IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  HEROKU_IMAGE_TAG: $HEROKU_REGISTRY/$HEROKU_APP/$HEROKU_PROCESS_TYPE
  SSH_ARGS: -o StrictHostKeyChecking=no $EC2_ADDRESS

cache:
  paths:
    - ./.m2/repository

before_script:
  - chmod +x mvnw

Build:
  stage: build
  when: on_success
  script:
    - ./mvnw $MAVEN_OPTS install -Dmaven.test.skip -Djacoco.skip -Dpmd.skip -Dcheckstyle.skip
  artifacts:
    paths:
      - './Dockerfile'
      - '*/target'
    expire_in: 2 weeks

Code quality check:
  stage: check
  script:
    - ./mvnw $MAVEN_OPTS checkstyle:check pmd:check detekt:check

Tests:
  stage: check
  script:
    - ./mvnw $MAVEN_OPTS test
    - awk -F"," '{ instructions += $4 + $5; covered += $5 } END { print "Total:", covered, "/", instructions,
      " instructions covered"; print "Total:", 100*covered/instructions, "% covered" }' ortube-test-coverage-report/target/site/jacoco-aggregate/jacoco.csv
  coverage: '/Total: (\d+\.+\d+\s{1}\%{1}\s{1})covered/'
  artifacts:
    reports:
      junit:
        - '*/target/surefire-reports/TEST-*.xml'
        - '*/target/failsafe-reports/TEST-*.xml'

Docker package:
  stage: docker-package
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  when: on_success
  cache: {}
  variables:
    GIT_STRATEGY: none
  before_script:
    - docker login -u $CI_REGISTRY_USR -p $CI_REGISTRY_PWD $CI_REGISTRY
  script:
    - docker build -t $CI_IMAGE_TAG .
    - docker push $CI_IMAGE_TAG
  only:
    - master
    - develop
    - tags

Heroku Deploy:
  stage: deploy-heroku
  when: manual
  image: registry.gitlab.com/ortube-app/docker-images/heroku-cli-docker
  services:
    - docker:19.03.1-dind
  cache: {}
  variables:
    GIT_STRATEGY: none
  before_script:
    - docker login -u $CI_REGISTRY_USR -p $CI_REGISTRY_PWD $CI_REGISTRY
    - docker login -u $HEROKU_REGISTRY_USR -p $HEROKU_API_KEY $HEROKU_REGISTRY
  script:
    - docker pull $CI_IMAGE_TAG
    - docker tag $CI_IMAGE_TAG $HEROKU_IMAGE_TAG
    - docker push $HEROKU_IMAGE_TAG
    - heroku container:release $HEROKU_PROCESS_TYPE -a $HEROKU_APP
  only:
    - master
    - develop
    - tags

Docker deploy AWS nonprod:
  stage: deploy-aws-ec2
  image: ubuntu:18.04
  when: manual
  cache: {}
  variables:
    GIT_STRATEGY: none
  before_script:
    - apt-get update && apt-get install openssh-client -y
    - chmod 400 $ORTUBE_EC2_PEM
  script:
    # Logging in to gitlab container registry
    - ssh -i $ORTUBE_EC2_PEM $SSH_ARGS docker login -u $CI_REGISTRY_USR -p $CI_REGISTRY_PWD $CI_REGISTRY
    # Trying to delete started container or suppress error if such container don't exist
    - ssh -i $ORTUBE_EC2_PEM $SSH_ARGS docker stop $CONTAINER_NAME || echo "Container don't started yet"
    # Trying to delete image of current branch or suppress error if such image don't pulled yet
    - ssh -i $ORTUBE_EC2_PEM $SSH_ARGS docker rmi $CI_IMAGE_TAG || echo "Image don't exists yes"
    # Run container from image of current branch
    - >
      ssh -i $ORTUBE_EC2_PEM $SSH_ARGS
      docker run $DOCKER_ARGS
      -eMONGODB_URI=\"$MONGODB_URI\"
      -eJWT_SECRET=$JWT_SECRET
      -eSENDER_PASSWORD=$SENDER_PASSWORD
      -eSENDER_USERNAME=$SENDER_USERNAME
      $CI_IMAGE_TAG
    # Removing all unused images
    - ssh -i $ORTUBE_EC2_PEM $SSH_ARGS docker image prune -af
  only:
    - master
    - develop
    - tags